
import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import GoogleMobileAds
import TabBarBox
import AudioToolbox
import SVProgressHUD
import SystemConfiguration
import Firebase
import FirebaseMessaging
import UserNotifications
import Branch
import Reachability
import SafariServices

var userDef = UserDefaults.standard
var reach : Reachability = Reachability()!

var isCallDataOnce = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate , BranchDeepLinkingController{
    
    
    
    
    var deepLinkingCompletionDelegate: BranchDeepLinkingControllerCompletionDelegate!
    

    var homeButtonVar = HomeButton()
    
    var window: UIWindow?
    var selectedCat : Int = 0
    var checkBucket : String = ""
    var badge_count : Int = 0
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "598125653281-i8pb8kakduiark5paqijrsg3ra40bh2n.apps.googleusercontent.com"
        IQKeyboardManager.shared.enable = true
        SVProgressHUD.setForegroundColor(GlobalConstant.hexStringToUIColor("0294D6"))
       
        // Override point for customization after application launch.
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    OperationQueue.main.addOperation {
                        UIApplication.shared.applicationIconBadgeNumber = 0
                        UIApplication.shared.registerForRemoteNotifications()
                        
                    }
                }
                else{
                    //Do stuff if unsuccessful...
                }
               
            })
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil);
            UIApplication.shared.registerUserNotificationSettings(setting);
            UIApplication.shared.registerForRemoteNotifications();
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.MessagingRegistrationTokenRefreshed, object: nil)
        
        FirebaseApp.configure()
        
        
        
        
        let session = userDef.object(forKey: "session") as? String
        if(session == "" || session == nil){
            isCallDataOnce = false
        }else{
            isCallDataOnce = true
        }
        
        
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            if error == nil && params!["+clicked_branch_link"] != nil{
                let sessionParams = Branch.getInstance().getLatestReferringParams()
                print(sessionParams!)
            } else {
            }
        })
        return true
    }
    
    
    
    func configureControl(withData params: [AnyHashable: Any]!) {
        let dict = params as Dictionary
        print(dict)
    }
  

    
    func handleDeepLinkParams(params : [String: AnyObject]){
            print("pram----\(params)")

       
//        let navigationController = window?.rootViewController as? UINavigationController
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let controller = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as? PostDetailViewController
//        navigationController?.pushViewController(controller!, animated: true)
        

//        NSString *deeplinkText = [params objectForKey:@"deeplink_text"];
//        if ([params[BRANCH_INIT_KEY_CLICKED_BRANCH_LINK] boolValue]) {
//
//            UINavigationController *navigationController =
//                (UINavigationController *)self.window.rootViewController;
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            LogOutputViewController *logOutputViewController =
//                [storyboard instantiateViewControllerWithIdentifier:@"LogOutputViewController"];
//            [navigationController pushViewController:logOutputViewController animated:YES];
//            NSString *logOutput =
//                [NSString stringWithFormat:@"Successfully Deeplinked:\n\n%@\nSession Details:\n\n%@",
//                    deeplinkText, [[[Branch getInstance] getLatestReferringParams] description]];
//            logOutputViewController.logOutput = logOutput;
//
//        } else {
//            NSLog(@"Branch TestBed: Finished init with params\n%@", params.description);
//        }
    }
    
    
   
    
    
//    - (void) handleDeepLinkObject:(BranchUniversalObject*)object
//    linkProperties:(BranchLinkProperties*)linkProperties
//    error:(NSError*)error {
//    if (error) {
//    NSLog(@"Branch TestBed: Error deep linking: %@.", error.localizedDescription);
//    return;
//    }
//
//    NSLog(@"Deep linked with object: %@.", object);
//    NSString *deeplinkText = object.contentMetadata.customMetadata[@"deeplink_text"];
//    if (object.contentMetadata.customMetadata[BRANCH_INIT_KEY_CLICKED_BRANCH_LINK].boolValue) {
//    UINavigationController *navigationController =
//    (UINavigationController *)self.window.rootViewController;
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    LogOutputViewController *logOutputViewController =
//    [storyboard instantiateViewControllerWithIdentifier:@"LogOutputViewController"];
//    [navigationController pushViewController:logOutputViewController animated:YES];
//    NSString *logOutput =
//    [NSString stringWithFormat:@"Successfully Deeplinked:\n\n%@\nSession Details:\n\n%@",
//    deeplinkText, [[[Branch getInstance] getLatestReferringParams] description]];
//    logOutputViewController.logOutput = logOutput;
//    }
//    }
//
//    - (void)onboardUserOnInstall {
//    NSURL *urlForOnboarding = [NSURL URLWithString:@"http://example.com"]; // Put your onboarding link here
//
//    id notInstall = [[NSUserDefaults standardUserDefaults] objectForKey:@"notInstall"];
//    if (!notInstall) {
//    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:@"notInstall"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    Branch *branch = [Branch getInstance];
//
//    // Note that this must be invoked *before* initSession
//    [branch enableDelayedInit];
//
//    NSURL *updatedUrlForOnboarding = [branch getUrlForOnboardingWithRedirectUrl:urlForOnboarding.absoluteString];
//    if (updatedUrlForOnboarding) {
//    // replace url for onboarding with the URL provided by Branch
//    urlForOnboarding = updatedUrlForOnboarding;
//    }
//    else {
//    // do not replace url for onboarding
//    NSLog(@"Was unable to get onboarding URL from Branch SDK, so proceeding with normal onboarding URL.");
//    [branch disableDelayedInit];
//    }
//
//    self.onboardingVC = [[SFSafariViewController alloc] initWithURL:urlForOnboarding];
//    self.onboardingVC.delegate = self;
//    dispatch_async(dispatch_get_main_queue(), ^{
//    [[[[UIApplication sharedApplication].delegate window] rootViewController]
//    presentViewController:self.onboardingVC animated:YES completion:NULL];
//    });
//    }
//    }
//
//    - (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
//    [[Branch getInstance] resumeInit];
//    }
//
//
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
         if(url.scheme == "https"){
            
            Branch.getInstance().application(app, open: url, options: options)
            
//            let post_id = getQueryStringParameter(url: "\(url)", param: "post_id")
//            
//            print("postid--\(post_id)")
//            
//            let navigationController = window?.rootViewController as? UINavigationController
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let controller = storyBoard.instantiateViewController(withIdentifier: "PostDetailViewController") as? PostDetailViewController
//            controller?.strPostId = post_id
//            navigationController?.pushViewController(controller!, animated: true)
            
            
             return true
        }
        else{
            return GIDSignIn.sharedInstance().handle(url as URL?,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        
        
        return false
        
    }
    
    func vibrateMobile() -> Void {
        
         AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    
    ////Handle push notification
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]){
        
        Branch.getInstance().handlePushNotification(userInfo)
    
        print("userinfo = \(userInfo)")
        
        let dataDict = userInfo as NSDictionary
        let notificationDict = dataDict["notification"] as! NSDictionary
            
        
        let title = notificationDict["title"] as! String
        let notification_type = notificationDict["notification_type"] as! String
        let body = notificationDict["body"] as! String
        badge_count = Int(notificationDict["badge_count"] as! NSNumber)
        
        
        if (application.applicationState == UIApplicationState.inactive || application.applicationState == UIApplicationState.background) {
            if #available(iOS 10.0, *){
                
                self.callLocalNotification(msgTitle:title , msgBody : body , notiFicationType:notification_type)
                
            }
            else{
                if (notification_type == "Bookmark") {
                    let navigationController = window?.rootViewController as? UINavigationController
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let controller = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
                    navigationController?.pushViewController(controller!, animated: true)
                }
                else{
                    //when tap on like notification
                }
            }
        }
       
        
        
//        {
//            "to":"dTxhWo6Mvpk:APA91bGLdwL08yGi0QCfso4yrjn0t4ISELVO9xvwHOPr0ChvoSlrdbu4MA4BN8hKz-uVMglNi7jvXZWNFPQtKJhxXOgXJugo-ZxFltlo2YF3l_DEF6GQ5w5SiwUWEWZ68jYzBPX_GYYA",
//            "priority":"high",
//            "notification":{
//                "":"Save Bookmark",
//                "":"Bookmark",
//                "user_id":144,
//                "post_id":123,
//                "post_type":1,
//                "message":"bookmark post name ",
//                "":"bookmark post name ",
//                "sound":"default",
//                "badge":1,
//                "content-available":1,
//                "":2,
//                "color":"#203E78"
//            }
//        }
        
    }
    
    
    func callLocalNotification(msgTitle:String , msgBody : String , notiFicationType:String){
        if(notiFicationType == "Comment") {
            if #available(iOS 10.0, *){
                //UNUserNotificationCenter.current().delegate = self
                let content = UNMutableNotificationContent()
                let requestIdentifier = "actionTap"
                content.categoryIdentifier = "CategoryIdentifier"
                content.sound = UNNotificationSound.default()
                content.badge = badge_count as NSNumber
                content.title = msgTitle
                content.body = msgBody
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
                let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error:Error?) in
                    if error != nil{
                        print(error?.localizedDescription ?? 0)
                    }
                    print("Notification Register Success")
                    
                    UIApplication.shared.applicationIconBadgeNumber = self.badge_count
                }
            }
        }
        else if(notiFicationType == "Like") {
            if #available(iOS 10.0, *){
                //UNUserNotificationCenter.current().delegate = self
                let content = UNMutableNotificationContent()
                let requestIdentifier = "actionTap"
                content.categoryIdentifier = "CategoryIdentifier"
                content.sound = UNNotificationSound.default()
                content.badge = badge_count as NSNumber
                content.title = msgTitle
                content.body = msgBody
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
                let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error:Error?) in
                    if error != nil{
                        print(error?.localizedDescription ?? 0)
                    }
                    print("Notification Register Success")
                    UIApplication.shared.applicationIconBadgeNumber = self.badge_count
                }
            }
        }
        else if(notiFicationType == "Bookmark") {
            if #available(iOS 10.0, *){
                //UNUserNotificationCenter.current().delegate = self
                let content = UNMutableNotificationContent()
                let requestIdentifier = "actionTap"
                content.categoryIdentifier = "CategoryIdentifier"
                content.sound = UNNotificationSound.default()
                content.badge = badge_count as NSNumber
                content.title = msgTitle
                content.body = msgBody
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
                let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error:Error?) in
                    if error != nil{
                        print(error?.localizedDescription ?? 0)
                    }
                    print("Notification Register Success")
                }
            }
        }
        else if(notiFicationType == "Trending") {
            if #available(iOS 10.0, *){
                //UNUserNotificationCenter.current().delegate = self
                let content = UNMutableNotificationContent()
                let requestIdentifier = "actionTap"
                content.categoryIdentifier = "CategoryIdentifier"
                content.sound = UNNotificationSound.default()
                content.badge = badge_count as NSNumber
                content.title = msgTitle
                content.body = msgBody
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
                let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error:Error?) in
                    if error != nil{
                        print(error?.localizedDescription ?? 0)
                    }
                    print("Notification Register Success")
                }
            }
        }
        else if(notiFicationType == "Notification") {
            if #available(iOS 10.0, *){
                //UNUserNotificationCenter.current().delegate = self
                let content = UNMutableNotificationContent()
                let requestIdentifier = "actionTap"
                content.categoryIdentifier = "CategoryIdentifier"
                content.sound = UNNotificationSound.default()
                content.badge = badge_count as NSNumber
                content.title = msgTitle
                content.body = msgBody
                let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 3.0, repeats: false)
                let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error:Error?) in
                    if error != nil{
                        print(error?.localizedDescription ?? 0)
                    }
                    print("Notification Register Success")
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var deviceTokenString = ""
        for i in 0..<deviceToken.count {
            deviceTokenString = deviceTokenString + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        print("devicetoken \(deviceTokenString)")
        
        
        Messaging.messaging()
            .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.unknown)
        
        userDef.set(deviceTokenString, forKey: "user_deviceToken")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(application.applicationState.rawValue)
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        debugPrint("handleEventsForBackgroundURLSession: \(identifier)")
        completionHandler()
    }
    
    

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound ,.badge])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.content.categoryIdentifier == "CategoryIdentifier" {
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                // print(response.actionIdentifier)
                self.didTapOnNotification()
                
                completionHandler()
            case "actionTap":
                //  print(response.actionIdentifier)
                self.didTapOnNotification()
                completionHandler()
            default:
                break;
            }
        }
        completionHandler()
    }
    
    func didTapOnNotification(){
       
    }
    
   
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    
    @objc func tokenRefreshNotification(notification: NSNotification)
    {
        
        if let refreshedToken = Messaging.messaging().fcmToken {
            print("InstanceID token: \(refreshedToken)")
            userDef.setValue(refreshedToken, forKey: "fcmtoken")
        }
        connectToFcm()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
   
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.connectToFcm()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func connectToFcm() {
        Messaging.messaging()
    }
    
    
    //// IPV6 config code
    func ipv6Reachability() -> SCNetworkReachability?{
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

}
extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}

//deep link
extension UIApplication{
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return application(app, open: url,
                           sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
    }
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//
//        print(url)
//        let arry : NSMutableArray = []
////        if(url.scheme == "applinks"){
////
////        }
//        if(url.scheme == "applinks"){
//            UIApplication.shared.open(url) { success in
//                if success {
//                    print("The URL was delivered successfully.")
//                } else {
//                    guard let url = URL(string: "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1251879389&mt=8") else { return }
//                    UIApplication.shared.open(url)
//                }
//            }}
//        else {
//            print("Invalid URL specified.")
//        }
//
//        //        if ([url.scheme isEqualToString:@"applinks"]) {
//        //            if ([url.lastPathComponent isEqualToString:@"Category_item_Share"]) {
//        //
//        //                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//        //
//        //
//        //                NSLog(@"%@",url);
//        //                NSString *urlShare=[NSString stringWithFormat:@"%@",url];
//        //
//        //
//        //
//        //
//        //                NSArray *comp1 = [urlShare componentsSeparatedByString:@"?"];
//        //                NSString *query = [comp1 lastObject];
//        //
//        //                NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
//        //                NSArray *urlComponents = [query componentsSeparatedByString:@"&"];
//        //
//        //                for (NSString *keyValuePair in urlComponents)
//        //                {
//        //                    NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
//        //                    NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
//        //                    NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
//        //
//        //                    [queryStringDictionary setObject:value forKey:key];
//        //                }
//        //
//        //
//        //                [arry addObject:queryStringDictionary];
//        //                NSLog(@"%@",arry);
//        //            }
//        //            UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
//        //            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//        //            categoriesViewController *controller = (categoriesViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"categoriesViewController"];
//        //            controller.arrAppdelegate=arry;
//        //            controller.checkAppdelegate=@"check";
//        //            [navigationController pushViewController:controller animated:YES];
//        //
//        //        }
//
//
//
//        //        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
//        //            // Handle the deep link. For example, show the deep-linked content or
//        //            // apply a promotional offer to the user's account.
//        //            // ...
//        //            return true
//        //        }
//        return true
//    }
  
    
   
    
// Respond to URI scheme links
func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    // pass the url to the handle deep link call
    
    
    let branchHandled = Branch.getInstance().application(application,
                                                         open: url,
                                                         sourceApplication: sourceApplication,
                                                         annotation: annotation
    )
    if (!branchHandled) {
        // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
    }
    
    // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
    return true
}

    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
    // pass the url to the handle deep link call
    Branch.getInstance().continue(userActivity)
    
    return true
    }
}


extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
