//
//  TableViewCell.swift
//  VCARDY
//
//  Created by Technorizen on 8/10/17.
//  Copyright © 2017 Technorizen. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var btnContact: UIButton!
    @IBOutlet var btnMore: UIButton!
    
    @IBOutlet weak var lblView: UIButton!
    @IBOutlet var btnBookmark: UIButton!
    @IBOutlet var collectionview: UICollectionView!
    @IBOutlet var constHeightCollection: NSLayoutConstraint!
    
    // Profile TimeLine
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var lblSeeMore: UILabel!
    @IBOutlet var imgBG: UIImageView!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnReply: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var lblDescription: UITextView!
    @IBOutlet var btnViewAll: UIButton!
    @IBOutlet var lblComment: UITextView!
    @IBOutlet var viewComment: UIView!
    @IBOutlet var lblVoteCount: UILabel!
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var optionTextField: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtViewHeight : NSLayoutConstraint!
    
    @IBOutlet var imgTop: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeight : NSLayoutConstraint!
    
    @IBOutlet var viewBottom: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUserDataOnPost(user_Detail : NSDictionary)  {
        
        print(user_Detail)
        
        let user_detail = user_Detail["user_detail"] as! NSDictionary
//        imgProfile.af_setImage(withURL: URL.init(string: user_detail["image"] as! String)!, placeholderImage:#imageLiteral(resourceName: "profilePlaceholder.jpg"))
        lblName.text = user_detail["username"] as? String
        let  createdDate : Date = GlobalConstant.getDateFromString(strDate: user_Detail["date_time"] as! String)
        lblDateTime.text = Date().offset(from: createdDate)
        lblComment.text = ""
        btnViewAll.setTitle("View all comment", for: .normal)
        if (user_Detail["cmt_list"] as! NSArray).count != 0 {
            let dict = (user_Detail["cmt_list"] as! NSArray).lastObject as! NSDictionary
            let myString = "\(dict["user"] as! String) \(dict["comment"] as! String)"
            var myMutableString = NSMutableAttributedString()
            myMutableString = NSMutableAttributedString(string: myString , attributes: [NSAttributedStringKey.font:UIFont(name: "Helvetica Neue", size: 14)!])
            myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: GlobalConstant.hexStringToUIColor("014477"), range: NSRange(location:0,length:(dict["user"] as! String).count))
            // set label Attribute
            lblComment.attributedText = myMutableString
            btnViewAll.setTitle("View all \((user_Detail["cmt_list"] as! NSArray).count) comment", for: .normal)
        }
        
        btnLike.setImage(UIImage.init(named: "unlike.png"), for: .normal)
        if (user_Detail["like"] as! Int) == 1 {
            btnLike.setImage(UIImage.init(named: "like.png"), for: .normal)
        }
    }
    

}
