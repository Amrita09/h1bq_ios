//
//  CollectionViewCell.swift
//  VCARDY
//
//  Created by Technorizen on 8/10/17.
//  Copyright © 2017 Technorizen. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var bgImage: UIImageView!
    @IBOutlet var imgVerified: UIImageView!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var imgCrossIcon: UIImageView!
    
    // Create Card
    @IBOutlet var imgQRCode: UIImageView!
    @IBOutlet var lblJobTitle: UILabel!
    @IBOutlet var lbladdress: UILabel!
    @IBOutlet var lblMailaddress: UILabel!
    @IBOutlet var lblPhone: UILabel!
    @IBOutlet var lblFacebook: UILabel!
    @IBOutlet var lblTwitter: UILabel!
    @IBOutlet var lblWeb: UILabel!
    
    @IBOutlet var btnShare: UIButton!
    
}
