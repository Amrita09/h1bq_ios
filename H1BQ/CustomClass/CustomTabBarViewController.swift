//
//  CustomTabBarViewController.swift
//  Example
//
//  Created by Farhad on 11/1/17.
//  Copyright © 2017 Farhad. All rights reserved.
//

import UIKit
import TabBarBox


class CustomTabBarViewController: TabBarBoxController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appdelegate.homeButtonVar = homeButton
        homeButton.addTarget(self, action: #selector(didTapAction(_:)), for: .touchUpInside)
    }
    
    @objc func didTapAction(_ sender: Any) {
        // do someThing
        let compose = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
        self.present(compose, animated: true, completion: nil)
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    }
    
}
