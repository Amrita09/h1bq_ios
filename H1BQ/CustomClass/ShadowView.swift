//
//  File.swift
//  H1BQ
//
//  Created by mac on 09/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
class ShadowView: UIView {
    //Shadow
    @IBInspectable var shadowColor: UIColor = UIColor.black {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowOpacity: Float = 0.5 {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 3, height: 3) {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 15.0 {
        didSet {
            self.updateView()
        }
    }
    
    //Apply params
    func updateView() {
        self.layer.shadowColor = self.shadowColor.cgColor
        self.layer.shadowOpacity = self.shadowOpacity
        self.layer.shadowOffset = self.shadowOffset
        self.layer.shadowRadius = self.shadowRadius
    }
}



//@IBDesignable
//open class CustomUIView: UIView {
//
//    @IBInspectable
//    public var borderColor: UIColor = UIColor.clear {
//        didSet {
//            layer.borderColor = borderColor.cgColor
//        }
//    }
//
//    @IBInspectable
//    public var borderWidth: CGFloat = 0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
//
//    @IBInspectable
//    public var cornerRadius: CGFloat = 0 {
//        didSet {
//            layer.cornerRadius = cornerRadius
//        }
//    }
//    @IBInspectable
//    public var shadowColor: UIColor = UIColor.clear {
//        didSet {
//            layer.shadowColor = shadowColor.cgColor
//        }
//    }
//
//    @IBInspectable
//    public var shadowRadius: CGFloat = 0 {
//        didSet {
//            layer.shadowRadius = shadowRadius
//        }
//    }
//
//    @IBInspectable
//    public var opacity: Float = 0.0 {
//        didSet {
//            layer.shadowOpacity = opacity
//        }
//    }
//
//    @IBInspectable
//    public var offSet: CGSize = CGSize(width: 0, height: 0) {
//        didSet {
//            layer.shadowOffset = offSet
//        }
//    }
//}

