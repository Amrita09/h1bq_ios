//
//  GlobalConstant.swift
//  Quick Moves
//
//  Created by Technorizen on 6/14/17.
//  Copyright © 2017 Technorizen. All rights reserved.
//

import UIKit
import Reachability
import CoreLocation

let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

let appdelegate = UIApplication.shared.delegate as! AppDelegate
//let MainStoryboard:UIStoryboard =

//#if DEBUG
//print()
//#endif

class GlobalConstant: NSObject {
    
    struct ScreenSize
    {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    static let  AppName = "H1BQ"
    static let  BaseURL = "https://www.h1bq.com/webservice/User/"
    static let  ContactURL = "https://www.h1bq.com/webservice/Countact_us/"
    static let  PostURL = "https://www.h1bq.com/webservice/Post/"
    static let  SearchURL = "https://www.h1bq.com/webservice/Search/"
    static let  CommentURL = "https://www.h1bq.com/webservice/Comment/"
    static let  strGooglelUrl = "http://maps.googleapis.com/maps/api/geocode/json?"
    static let  notificationListUrl = "https://www.h1bq.com/webservice/User/notification_list"
    static let  clearNotificationUrl = "https://www.h1bq.com/webservice/User/notification_clear"
    static let  checkActiveORnot = "https://www.h1bq.com/webservice/User/check_user_id"

    static let Google_API_KEY  = "AIzaSyDderHNNXYg59NVoPvfC_NgGpQgh76S6qs"
    // Messages
    static let  MSGServerError = "Server have some error. Please try again letter!"
    static let  MSGCreateGroupContacts = "Please select at least one contact"
    
    static let Advertisment_URL = "https://www.h1bq.com/webservice/App/advertisement_list"
    
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0

    
//    class func GoHome() {
//            let tabBarController = ESTabBarController()
//            tabBarController.delegate = self
//            tabBarController.title = "Irregularity"
//            tabBarController.tabBar.shadowImage = UIImage(named: "transparent")
//            tabBarController.tabBar.backgroundImage = UIImage(named: "background_dark")
//            tabBarController.shouldHijackHandler = {
//                tabbarController, viewController, index in
//                if index == 2 {
//                    return true
//                }
//                return false
//            }
//            tabBarController.didHijackHandler = {
//                [weak tabBarController] tabbarController, viewController, index in
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                    let alertController = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
//                    let takePhotoAction = UIAlertAction.init(title: "Take a photo", style: .default) { _ in
//                        self.openCamera()
//                    }
//                    alertController.addAction(takePhotoAction)
//                    let selectFromAlbumAction = UIAlertAction.init(title: "Select from album", style: .default) { _ in
//                        self.openGallery()
//                    }
//                    alertController.addAction(selectFromAlbumAction)
//                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//                    alertController.addAction(cancelAction)
//                    tabBarController?.present(alertController, animated: true, completion: nil)
//                }
//            }
//
//            let v1 = MainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//            let v2 = MainStoryboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
////            let v3 = MainStoryboard.instantiateViewController(withIdentifier: "TimelineVC") as! TimelineVC
//            let v4 = MainStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
//            let v5 = MainStoryboard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//
//            v1.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: nil, image: UIImage(named: "home"), selectedImage: UIImage(named: "home"))
//            v2.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: nil, image: UIImage(named: "chat"), selectedImage: UIImage(named: "chat"))
//            v3.tabBarItem = ESTabBarItem.init(ExampleIrregularityContentView(), title: nil, image: UIImage(named: "add"), selectedImage: UIImage(named: "add"))
//            v4.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: nil, image: UIImage(named: "notification"), selectedImage: UIImage(named: "notification"))
//            v5.tabBarItem = ESTabBarItem.init(ExampleIrregularityBasicContentView(), title: nil, image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
//
//            tabBarController.viewControllers = [v1, v2, v3, v4, v5]
//
//            self.navigationController?.pushViewController(tabBarController, animated: true)
//
//    }
    
    class func showAlertMessage(withOkButtonAndTitle strTitle: String, andMessage strMessage: String, on controller: UIViewController) {
        
        let alertController = UIAlertController(title: strTitle, message: strMessage, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(ok)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func isReachable() -> Bool {
        
        let reach = Reachability(hostname: "www.google.com")
        return reach!.isReachable
    }
    
    class func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")
        return passwordTest.evaluate(with: password)
    }
    
    class func isValidEmail(_ testStr:String) -> Bool {
        
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = URL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    class func hexStringToUIColor (_ hex:String) -> UIColor {
        
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    class func convertDateStringFromDate(_ fromFormate : String, toFormate : String, strDate : String)-> String {
        
        let dateFormate : DateFormatter = DateFormatter()
        dateFormate.dateFormat = fromFormate
        let date : Date = dateFormate.date(from: strDate)!
        dateFormate.dateFormat = toFormate
        return dateFormate.string(from: date)
    }
    
    class func getStringFromDate()-> String {
        
        let dateFormate : DateFormatter = DateFormatter()
        dateFormate.dateFormat = "E MMM dd HH:mm:ss Z yyyy"
        dateFormate.timeZone = TimeZone.init(abbreviation: "UTC")
        return dateFormate.string(from: Date())
    }
    
    class func getDateFromString(strDate : String)-> Date {
        
        let dateFormate : DateFormatter = DateFormatter()
        dateFormate.dateFormat = "E MMM dd HH:mm:ss Z yyyy"
        return dateFormate.date(from: strDate)!
    }
    
    class func getDateFromStringWithFormate(strFormate : String,strDate : String)-> Date {
        
        let date = Date()
        if strDate != "" {
            
            let dateFormate : DateFormatter = DateFormatter()
            //dateFormate.timeZone = TimeZone(abbreviation: "UTC")
            dateFormate.dateFormat = strFormate
             dateFormate.dateFormat = "E MMM dd HH:mm:ss Z yyyy"
            let dt = dateFormate.date(from: strDate)
            //dateFormate.timeZone = TimeZone.current
            let convertDate = dateFormate.string(from: dt!)
            return dateFormate.date(from: convertDate)!
        }
        
        return date
    }
    
    class func validateIFSC(code : String) -> Bool {
        
        let regex = try! NSRegularExpression(pattern: "^[A-Za-z]{4}0.{6}$")
        return regex.numberOfMatches(in: code, range: NSRange(location:0, length: code.count)) == 1
    }
    
    class func getCountryCodeDictionary(strCode : String) -> String {
        
        let dict : NSDictionary = ["IL" : "972","AF" : "93","AL" : "355","DZ" : "213","AS" : "1","AD" : "376","AO" : "244","AI" : "1","AG" : "1","AR" : "54","AM" : "374","AW" : "297","AU" : "61","AT" : "43","AZ" : "994","BS" : "1","BH" : "973","BD" : "880","BB" : "1","BY" : "375","BE" : "32","BZ" : "501","BJ" : "229","BM" : "1","BT" : "975","BA" : "387","BW" : "267","BR" : "55","IO" : "246","BG" : "359","BF" : "226","BI" : "257","KH" : "855","CM" : "237","CA" : "1","CV" : "238","KY" : "345","CF" : "236","TD" : "235","CL" : "56","CN" : "86","CX" : "61","CO" : "57","KM" : "269","CG" : "242","CK" : "682","CR" : "506","HR" : "385","CU" : "53","CY" : "537","CZ" : "420","DK" : "45","DJ" : "253","DM" : "1","DO" : "1","EC" : "593","EG" : "20","SV" : "503","GQ" : "240","ER" : "291","EE" : "372","ET" : "251","FO" : "298","FJ" : "679","FI" : "358","FR" : "33","GF" : "594","PF" : "689","GA" : "241","GM" : "220","GE" : "995","DE" : "49","GH" : "233","GI" : "350","GR" : "30","GL" : "299","GD" : "1","GP" : "590","GU" : "1","GT" : "502","GN" : "224","GW" : "245","GY" : "595","HT" : "509","HN" : "504","HU" : "36","IS" : "354","IN" : "91","ID" : "62","IQ" : "964","IE" : "353","IL" : "972","IT" : "39","JM" : "1","JP" : "81","JO" : "962","KZ" : "77","KE" : "254","KI" : "686","KW" : "965","KG" : "996","LV" : "371","LB" : "961","LS" : "266","LR" : "231","LI" : "423","LT" : "370","LU" : "352","MG" : "261","MW" : "265","MY" : "60","MV" : "960","ML" : "223","MT" : "356","MH" : "692","MQ" : "596","MR" : "222","MU" : "230","YT" : "262","MX" : "52","MC" : "377","MN" : "976","ME" : "382","MS" : "1","MA" : "212","MM" : "95","NA" : "264","NR" : "674","NP" : "977","NL" : "31","AN" : "599","NC" : "687","NZ" : "64","NI" : "505","NE" : "227","NG" : "234","NU" : "683","NF" : "672","MP" : "1","NO" : "47","OM" : "968","PK" : "92","PW" : "680","PA" : "507","PG" : "675","PY" : "595","PE" : "51","PH" : "63","PL" : "48","PT" : "351","PR" : "1","QA" : "974","RO" : "40","RW" : "250","WS" : "685","SM" : "378","SA" : "966","SN" : "221","RS" : "381","SC" : "248","SL" : "232","SG" : "65","SK" : "421","SI" : "386","SB" : "677","ZA" : "27","GS" : "500","ES" : "34","LK" : "94","SD" : "249","SR" : "597","SZ" : "268","SE" : "46","CH" : "41","TJ" : "992","TH" : "66","TG" : "228","TK" : "690","TO" : "676","TT" : "1","TN" : "216","TR" : "90","TM" : "993","TC" : "1","TV" : "688","UG" : "256","UA" : "380","AE" : "971","GB" : "44","US" : "1","UY" : "598","UZ" : "998","VU" : "678","WF" : "681","YE" : "967","ZM" : "260","ZW" : "263","BO" : "591","BN" : "673","CC" : "61","CD" : "243","CI" : "225","FK" : "500","GG" : "44","VA" : "379","HK" : "852","IR" : "98","IM" : "44","JE" : "44","KP" : "850","KR" : "82","LA" : "856","LY" : "218","MO" : "853","MK" : "389","FM" : "691","MD" : "373","MZ" : "258","PS" : "970","PN" : "872","RE" : "262","RU" : "7","BL" : "590","SH" : "290","KN" : "1","LC" : "1","MF" : "590","PM" : "508","VC" : "1","ST" : "239","SO" : "252","SJ" : "47","SY" : "963","TW" : "886","TZ" : "255","TL" : "670","VE" : "58","VN" : "84","VG" : "1","VI" : "1"]
        return dict.value(forKey: "\(strCode)") as! String
    }
    
}
