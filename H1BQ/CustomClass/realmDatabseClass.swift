//
//  realmDatabseClass.swift
//  H1BQ
//
//  Created by Rashida on 31/12/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import RealmSwift

class PostListRealm: Object {
    @objc dynamic var add_date: String = ""
    @objc dynamic var ans_selection_typ: Int = 0
    @objc dynamic var bookmark_status: Int = 0
    @objc dynamic var bucket: Int = 0
    @objc dynamic var count_comment: Int = 0
    @objc dynamic var count_like: Int = 0
    @objc dynamic var count_view: Int = 0
    @objc dynamic var count_vote: Int = 0
    
    @objc dynamic var like_status: Int = 0
    @objc dynamic var post_id: Int = 0
    @objc dynamic var post_type: Int = 0
    @objc dynamic var report_abuse_status: Int = 0
    
    @objc dynamic var trending_status: Int = 0
    @objc dynamic var user_id: Int = 0
    @objc dynamic var view_status: Int = 0
    
    @objc dynamic var dicription: String = ""
    @objc dynamic var post_image: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var username: String = ""
    
    //@objc dynamic var answer: NSArray = []
    
    override static func primaryKey() -> String? {
        return "post_id"
    }
}






