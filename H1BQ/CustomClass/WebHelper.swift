//
//  WebHelper.swift
//  SchoolApp
//
//  Created by Technorizen on 4/29/17.
//  Copyright © 2017 Technorizen. All rights reserved.
//

import UIKit
import SVProgressHUD
class WebHelper: NSObject {
    
    
    class func requestPostUrl(_ strURL: String,dictParameter: NSDictionary, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        SVProgressHUD.dismiss()
        SVProgressHUD.show()
        if GlobalConstant.isReachable() {
            let session = URLSession.shared
            let postData = try? JSONSerialization.data(withJSONObject: dictParameter, options: .prettyPrinted)
            let saveString = String(data: postData!, encoding: String.Encoding.utf8)
            print(saveString!);
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            //For first name
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"first_name\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["first_name"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For last_name
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"last_name\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["last_name"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For username
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"username\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["username"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For email
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For password
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"password\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["password"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For country
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"country\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["country"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For zipcode
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"zipcode\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["zipcode"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For sigup_type
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"sigup_type\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["sigup_type"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For deviceid
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"deviceid\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["deviceid"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For devicetype
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"devicetype\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["devicetype"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For gender
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"gender\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["gender"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    let _: NSError?
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    
    class func requestForUpdateProfilePostUrl(_ strURL: String,dictParameter: NSDictionary, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        SVProgressHUD.show()
        if GlobalConstant.isReachable() {
            let session = URLSession.shared
            let postData = try? JSONSerialization.data(withJSONObject: dictParameter, options: .prettyPrinted)
            let saveString = String(data: postData!, encoding: String.Encoding.utf8)
            print(saveString!);
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            //For first name
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"first_name\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["first_name"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For last_name
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"last_name\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["last_name"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For user_id
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For email
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            
            //For country
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"countray\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["country"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For zipcode
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"zipcode\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["zipcode"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            /*//For profile_file
             body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
             body.append("Content-Disposition:form-data; name=\"profile_file\"".data(using: String.Encoding.utf8)!)
             body.append("\r\n\r\n".data(using: String.Encoding.utf8)!)
             body.append("\r\n".data(using: String.Encoding.utf8)!)*/
            
            
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    let _: NSError?
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    class func requestForCheckUsernamePostUrl(_ strURL: String,dictParameter: NSDictionary, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        SVProgressHUD.show()
        if GlobalConstant.isReachable() {
            let session = URLSession.shared
            let postData = try? JSONSerialization.data(withJSONObject: dictParameter, options: .prettyPrinted)
            let saveString = String(data: postData!, encoding: String.Encoding.utf8)
            print(saveString!);
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            if dictParameter["old_password"] is String {
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For old_password
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"old_password\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["old_password"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For new_password
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"new_password\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["new_password"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
            }
                
            else if dictParameter["full_name"] is String {
                //For full_name
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"full_name\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["full_name"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For email
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For regarding
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"regarding\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["regarding"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For descripation
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"descripation\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["descripation"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For mobile
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"full_name\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["mobile"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
                
            else if dictParameter["user_id"] is String {
                //For username
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            else if (dictParameter["Username"] as! String) != "" {
                //For username
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"Username\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["Username"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            
            
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    let _: NSError?
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    class func requestForCheck_social_email(_ strURL: String,dictParameter: NSDictionary, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        SVProgressHUD.show()
        if GlobalConstant.isReachable() {
            let session = URLSession.shared
            let postData = try? JSONSerialization.data(withJSONObject: dictParameter, options: .prettyPrinted)
            let saveString = String(data: postData!, encoding: String.Encoding.utf8)
            print(saveString!);
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            //For email
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            
            
            
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    let _: NSError?
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    
    class func requestForReset_OTPPostUrl(_ strURL: String,dictParameter: NSDictionary, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
       // SVProgressHUD.dismiss()
       // SVProgressHUD.show()
        if GlobalConstant.isReachable() {
            let session = URLSession.shared
            let postData = try? JSONSerialization.data(withJSONObject: dictParameter, options: .prettyPrinted)
            let saveString = String(data: postData!, encoding: String.Encoding.utf8)
            print(saveString!);
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            if dictParameter["password"] is String {
                //For email
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For password
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"password\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["password"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For device_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"deviceid\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["deviceid"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For devicetype
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"devicetype\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["devicetype"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
                
            else if dictParameter["otp_reset_type"] is String {
                //For email
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For otp_reset_type
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"otp_reset_type\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["otp_reset_type"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            else if dictParameter["otp"] is String {
                //For email
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For otp_reset_type
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"otp\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["otp"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            else if dictParameter["password_otp"] is String {
                //For email
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"email\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["email"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For otp_reset_type
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"password_otp\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["password_otp"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            else if dictParameter["new_password"] is String {
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For new_password
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"new_password\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["new_password"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            else if dictParameter["comment_id"] is Int{
                
                SVProgressHUD.dismiss()
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For comment_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"comment_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["comment_id"] as! Int)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For like_cooment_type
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"like_cooment_type\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["like_cooment_type"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            else if dictParameter["post_id"] is String {
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For offset
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"offset\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["offset"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
            }
            else if dictParameter["offset"] is String {
              // jghghj
               
               
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For offset
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"offset\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["offset"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For buket_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"buket_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["buket_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    let _: NSError?
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    
    class func requestUrlForPostWithType(_ strURL: String,strType:String,dictParameter: NSDictionary, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        
        
        if GlobalConstant.isReachable() {
            
            let session = URLSession.shared
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL)
            
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            if strType == "Clear_Noti" {
                
                //For user_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            
            
            if strType == "Add_bookmark" {
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            
            if strType == "bookmark_list" {
                
                //For user_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For offset
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"offset\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["offset"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            
            if strType == "search" {
                //For user_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"get_txt\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["get_txt"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            if strType == "answe_submit" {
                //For user_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                
                //For anser_id[]
                for i in 0..<(dictParameter["anser_id"] as! NSArray).count {
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition:form-data; name=\"anser_id[]\"".data(using: String.Encoding.utf8)!)
                    body.append("\r\n\r\n\((dictParameter["anser_id"] as! NSArray)[i] as! Int)".data(using: String.Encoding.utf8)!)
                    body.append("\r\n".data(using: String.Encoding.utf8)!)
                }
                
            }
            
            if strType == "add_comment" {
                //For user_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For comment_type
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"comment_type\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["comment_type"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                if (dictParameter["comment_type"] as! String) == "2" {
                    // For comment_id
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition:form-data; name=\"comment_id\"".data(using: String.Encoding.utf8)!)
                    body.append("\r\n\r\n\(dictParameter["comment_id"] as! String)".data(using: String.Encoding.utf8)!)
                    body.append("\r\n".data(using: String.Encoding.utf8)!)
                }
                
                if dictParameter["nested_comment_id"] is String {
                    // For nested_comment_id
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition:form-data; name=\"nested_comment_id\"".data(using: String.Encoding.utf8)!)
                    body.append("\r\n\r\n\(dictParameter["nested_comment_id"] as! String)".data(using: String.Encoding.utf8)!)
                    body.append("\r\n".data(using: String.Encoding.utf8)!)
                }
                // For message
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"message\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["message"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // Image Data
                if let imageData = dictParameter["image"] as? Data{
                    let filename = "user-profile.jpg"
                    
                    let mimetype = "image/jpg"
                    
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition: form-data; name=\"image\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
                    body.append(imageData)
                    body.append("\r\n".data(using: String.Encoding.utf8)!)
                    
                    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
                    
                }
                //For date
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"add_date\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(GlobalConstant.getStringFromDate())".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            if strType == "like_post" {
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            if strType == "report_abous" {
                //For user_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For message
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"message\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["message"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            if strType == "delete_post"   {
                // For post_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
            }
            if strType == "poll_detail"   {
                // For post_id
                SVProgressHUD.show()
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
            }
            if strType == "load_nested_comment" {
                
                SVProgressHUD.show()
                // For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                // For comment_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"comment_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["comment_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    let _: NSError?
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    class func requestGetUrl(_ strURL: String, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        
        if GlobalConstant.isReachable() {
            let session = URLSession.shared
            
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    if let httpResponse = response as? HTTPURLResponse {
                        print("error \(httpResponse.statusCode)")
                        if httpResponse.statusCode == 200 || httpResponse.statusCode == 208 {
                            _ = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                            let _: NSError?
                            let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                            
                            if (jsonResult is NSDictionary) {
                                success(jsonResult as! [AnyHashable : Any])
                            }
                            else{
                                DispatchQueue.main.async {
                                    SVProgressHUD.dismiss()
                                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                                }
                            }
                        }
                        else{
                            failure(error)
                        }
                    }
                }
                
            })
            
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    class func requestPostUrlWithoutImage(_ strURL: String, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        SVProgressHUD.show()
        if GlobalConstant.isReachable() {
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(urlwithPercentEscapes!);
            let url = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: url!)
            request.httpMethod = "POST"
            
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    let response = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    print(response!)
                    let _: NSError?
                    let jsonResult: NSDictionary = try! JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    success(jsonResult as! [AnyHashable : Any])
                }
                
            })
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    
    class func requestCreatePostUrlWithImage(_ strURL: String,dictParameter: NSDictionary,strType:String,imageParamName: String,imageData:Data?, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        SVProgressHUD.show()
        
        if GlobalConstant.isReachable() {
            
            let session = URLSession.shared
            let postData = try? JSONSerialization.data(withJSONObject: dictParameter, options: .prettyPrinted)
            let saveString = String(data: postData!, encoding: String.Encoding.utf8)
            print(saveString!);
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            if strType == "add_post" {
                //For user_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                //For add_date
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"add_date\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(GlobalConstant.getStringFromDate())".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            else{
                //For post_id
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"post_id\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(dictParameter["post_id"] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            
            //For post_type
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"post_type\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["post_type"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For bucket
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"bucket\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["bucket"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For title
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"title\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["title"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For dicription
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"dicription\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["dicription"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            
            //            let fname = "test654321.png"
            //            let mimetype = "image/png"
            //
            //            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            //            body.append("Content-Disposition:form-data; name=\"\(imageParamName)\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            //            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            //            body.append(imageData)
            //            body.append("\r\n".data(using: String.Encoding.utf8)!)
            if imageParamName != ""{
                let filename = "user-profile.jpg"
                
                let mimetype = "image/jpg"
                
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(imageParamName)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
                
                body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
                
                
            }
            request.httpBody = body as Data
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    let response = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    print(response!)
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
    class func requestCreatePoll(_ strURL: String,dictParameter: NSDictionary,arrOptions : NSArray, controllerView viewController: UIViewController, success: @escaping (_ response: [AnyHashable: Any]) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        
        SVProgressHUD.dismiss()
        SVProgressHUD.show()
        if GlobalConstant.isReachable() {
            let session = URLSession.shared
            let postData = try? JSONSerialization.data(withJSONObject: dictParameter, options: .prettyPrinted)
            let saveString = String(data: postData!, encoding: String.Encoding.utf8)
            print(saveString!);
            let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            print(strURL);
            let urlPath = URL(string: urlwithPercentEscapes!)
            
            let request = NSMutableURLRequest(url: urlPath! as URL)
            request.timeoutInterval = 60
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
            request.httpMethod = "POST"
            let boundary = "Boundary-\(UUID().uuidString)"
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            let body = NSMutableData()
            
            //For user_id
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"user_id\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["user_id"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For add_date
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"add_date\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(GlobalConstant.getStringFromDate())".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For post_type
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"post_type\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["post_type"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For bucket
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"bucket\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["bucket"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For title
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"title\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["title"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            //For anser[]
            for i in 0..<arrOptions.count {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition:form-data; name=\"anser[]\"".data(using: String.Encoding.utf8)!)
                body.append("\r\n\r\n\(arrOptions[i] as! String)".data(using: String.Encoding.utf8)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            
            //For ans_selection_typ
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"ans_selection_typ\"".data(using: String.Encoding.utf8)!)
            body.append("\r\n\r\n\(dictParameter["ans_selection_typ"] as! String)".data(using: String.Encoding.utf8)!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.global(qos: .userInitiated).async {
                    
                    // Bounce back to the main thread to update the UI
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                }
                
                if((error) != nil) {
                    print(error!.localizedDescription)
                    failure(error)
                }else {
                    let response = NSString(data: data!, encoding:String.Encoding.utf8.rawValue)
                    print(response!)
                    let jsonResult = try? JSONSerialization.jsonObject(with: data!, options:    JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if (jsonResult is NSDictionary) {
                        success(jsonResult as! [AnyHashable : Any])
                    }
                    else{
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "JSON text did not start with array or object and option to allow fragments not set.", on: viewController)
                        }
                        
                    }
                }
                
            })
            task.resume()
        }
        else {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: "", andMessage: "Internet not connected", on: viewController)
            }
            
        }
    }
}

