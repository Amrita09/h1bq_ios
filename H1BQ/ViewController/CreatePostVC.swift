//
//  CreatePostVC.swift
//  H1BQ
//
//  Created by mac on 16/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import DropDown
class CreatePostVC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var dictPost : NSDictionary!
    var imagePicker = UIImagePickerController()
    var strBucket : String! = ""
    var arrPolls : [String] = ["H1B","PERM","I-140","I-131(AP)","I-485","I-765(EAD)","MISC"]
    @IBOutlet var btnPool: UIButton!
    @IBOutlet var postButton: UIButton!
    @IBOutlet var pollButton: UIButton!
    var strContentType : String! = "1"
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var polltitleTextField: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var postImageView: UIImageView!
    @IBOutlet var uploadImageView: UIImageView!
    @IBOutlet var postView: UIView!
    @IBOutlet var pollView: UIView!
    @IBOutlet var optionTableView: UITableView!
    
    @IBOutlet weak var btnEdit : UIButton!
    
    var arrOptions : NSMutableArray = [""]
    var strSelectionType : String! = "1"
    var selectedIndex = Int()
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var constHeightContentView: NSLayoutConstraint!
    var callback: ((Int) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.pollView.isHidden = true
        if dictPost != nil {
            
            btnEdit.isHidden = false
            contentView.isHidden = true
            constHeightContentView.constant = 0
            let bucketID =  dictPost["bucket"] as! Int
            strBucket = "\(bucketID)"
            btnPool.setTitle(arrPolls[bucketID - 1], for: .normal)
            btnPool.setTitleColor(UIColor.black, for: .normal)
            titleTextField.text = dictPost["title"] as? String
            
            let decodeText = dictPost["dicription"] as? String
            let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
            if let decoded = msg.fromBase64() {
                descriptionTextView.text = decoded
            }else{
                descriptionTextView.text = dictPost["dicription"] as? String
            }
            descriptionTextView.isEditable = false
            if dictPost["post_image"] as! String != "" {
                uploadImageView.af_setImage(withURL: URL.init(string: dictPost["post_image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
                postImageView.isHidden = true
            }
            
            
//            let text = "How technology is changing our relationships to each other: http://t.ted.com/mzRtRfX"
//
//
//
//            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//             let matches = detector.matches(in: text, options: .reportCompletion, range: NSMakeRange(0, text.count))
//                for match in matches {
//                    print(match.url!)
//
//                    let attributes = [[NSAttributedStringKey.foregroundColor:UIColor.blue], [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 15)]]
//                    descriptionTextView.attributedText = "How technology is changing our relationships to each other: text".highlightWordsIn(highlightedWords: "\(match.url!)", attributes: attributes)
//
//                }
//
         }
        else{
            btnEdit.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if timerTest != nil {
            timerTest?.invalidate()
            timerTest = nil
        }
    }
    
    @IBAction func clickOnBtnEdit(_ sender : UIButton){
        if sender.isSelected == true {
            sender.isSelected = false
            descriptionTextView.isEditable = false
            descriptionTextView.resignFirstResponder()
        }else {
            sender.isSelected = true
            descriptionTextView.isEditable = true
            descriptionTextView.becomeFirstResponder()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func regardingbtnTapped(_ sender: UIButton) {
        
        let dropDown = DropDown()
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = arrPolls
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedIndex = index
            self.strBucket = "\(index+1)"
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
        }
        
        dropDown.show()
    }
    @IBAction func didTappedonContentType(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            postButton.setImage(#imageLiteral(resourceName: "radio_active"), for: .normal)
            pollButton.setImage(#imageLiteral(resourceName: "radio_inactive"), for: .normal)
            strContentType = "1"
            self.pollView.isHidden = true
            self.postView.isHidden = false
            descriptionTextView.isEditable = true
        case 2:
            pollButton.setImage(#imageLiteral(resourceName: "radio_active"), for: .normal)
            postButton.setImage(#imageLiteral(resourceName: "radio_inactive"), for: .normal)
            strContentType = "2"
            self.pollView.isHidden = false
            self.postView.isHidden = true
        default:
            postButton.setImage(#imageLiteral(resourceName: "radio_active"), for: .normal)
            pollButton.setImage(#imageLiteral(resourceName: "radio_inactive"), for: .normal)
            strContentType = "1"
            self.pollView.isHidden = true
            self.postView.isHidden = false
        }
    }
    @IBAction func didTappedonAnsSelectionType(_ sender: UIButton) {
        if sender.image(for: .normal) == UIImage.init(named: "uncheck_multiple") {
            strSelectionType = "2"
            sender.setImage(#imageLiteral(resourceName: "checked_multiple"), for: .normal)
        }
        else{
            strSelectionType = "1"
            sender.setImage(#imageLiteral(resourceName: "uncheck_multiple"), for: .normal)
        }
    }
    @IBAction func didTappedonUploadImage(_ sender: Any) {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: NSLocalizedString("Pick Image", comment: ""), message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { _ in
            print("Annuleer")
            self.dismiss(animated: true, completion: nil)
            
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title:  NSLocalizedString("Camera", comment: ""), style: .default)
        { _ in
            print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera;
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            else{
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Camera not available", on: self)
            }
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default)
        { _ in
            print("Galerij")
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            else{
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Gallery not available", on: self)
            }
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadImageView.image = image
            postImageView.isHidden = true
        } else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func postButtonSelect(_ sender: Any) {
        if isvalidateAllfield() == true {
            if dictPost != nil {
                self.updateOldPost()
            }
            else{
                self.submitNewPost()
            }
        }
    }
    func submitNewPost()  {
        var strParam = ""
        var data: Data? = nil
        if uploadImageView.image != nil {
            strParam = "image"
            data = UIImageJPEGRepresentation(uploadImageView.image!, 0.5)!
        }
        
        var encoded = descriptionTextView.text?.toBase64()
        let remainder = (encoded?.count)! % 4
        if remainder > 0 {
            encoded = encoded?.padding(toLength: (encoded?.count)! + 4 - remainder,
                            withPad: "=",
                            startingAt: 0)
            print(encoded!)
        }

        
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String,"post_type":strContentType,"bucket":strBucket,"title":titleTextField.text!,"dicription":encoded!]
        print(dictParam)
        
        WebHelper.requestCreatePostUrlWithImage("\(GlobalConstant.PostURL)add_post", dictParameter: dictParam, strType: "add_post", imageParamName: strParam, imageData: data, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] as! Int == 200 {
                    appdelegate.selectedCat = self.selectedIndex
                    DispatchQueue.main.async {
                        
                        self.dismiss(animated: true, completion: {
                           self.tabBarController?.selectedIndex = 0
                            appdelegate.checkBucket = "present"
                        })
                    }
                }
                else{
                    DispatchQueue.main.async {
                        
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    func updateOldPost()  {
        var strParam = ""
        var data: Data? = nil
        if uploadImageView.image != nil {
            strParam = "image"
            data = UIImageJPEGRepresentation(uploadImageView.image!, 0.5)!
        }
        var encoded = descriptionTextView.text?.toBase64()
        let remainder = (encoded?.count)! % 4
        if remainder > 0 {
            encoded = encoded?.padding(toLength: (encoded?.count)! + 4 - remainder,
                                       withPad: "=",
                                       startingAt: 0)
            print(encoded!)
        }
        let dictParam : NSDictionary = ["post_id":String.init(format: "%d", dictPost["post_id"] as! Int),"post_type":strContentType,"bucket":strBucket,"title":titleTextField.text!,"dicription":encoded!]
        
        WebHelper.requestCreatePostUrlWithImage("\(GlobalConstant.PostURL)edit_post", dictParameter: dictParam, strType: "edit_post", imageParamName: strParam, imageData: data, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] as! Int == 200 {
                    DispatchQueue.main.async {
                        
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    func isvalidateAllfield()-> Bool {
        if strBucket == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Pool is required.", on: self)
            return false
        }
        if titleTextField.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Title is required.", on: self)
            return false
        }
        if descriptionTextView.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Description is required.", on: self)
            return false
        }
        return true
    }
    @IBAction func pollButtonSelect(_ sender: Any) {
        
        if isvalidateAllfieldForPoll() == true {
            
            let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String,"post_type":strContentType,"bucket":strBucket,"title":polltitleTextField.text!,"ans_selection_typ":strSelectionType]
            print("MY PARAMS:", dictParam)
            
            WebHelper.requestCreatePoll("\(GlobalConstant.PostURL)add_post", dictParameter: dictParam, arrOptions: arrOptions, controllerView: self, success: { (response) in
                print(response)
                if response.count == 0 {
                    DispatchQueue.main.async {
                        
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                    }
                }
                else{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                }
            })
        }
    }
    func isvalidateAllfieldForPoll()-> Bool {
        if strBucket == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Bucket is required.", on: self)
            return false
        }
        if polltitleTextField.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Title is required.", on: self)
            return false
        }
        if arrOptions.contains("")  {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Option is required.", on: self)
            return false
        }
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CreatePostVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == titleTextField || textField == polltitleTextField {
            let maxLength = 40
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField != titleTextField && textField != polltitleTextField {
            arrOptions.replaceObject(at: textField.tag, with: textField.text!)
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField != titleTextField && textField != polltitleTextField {
            textField.text = ""
        }
        
    }
}
extension CreatePostVC : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 1000
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write here" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Write here"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension CreatePostVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        
        
        cell.selectionStyle = .none
        cell.optionTextField.delegate = self
        cell.optionTextField.tag = indexPath.row
         cell.optionTextField.placeholder = String.init(format: "Option %d", indexPath.row+1)
        
        if(arrOptions.count > 0){
            cell.optionTextField.text = arrOptions[indexPath.row] as? String
        }else{
            cell.optionTextField.text = ""
        }
       
        if(indexPath.row < 9){
            cell.btnContact.setImage(#imageLiteral(resourceName: "red_cross"), for: .normal)
            if indexPath.row == arrOptions.count-1 {
                cell.btnContact.setImage(#imageLiteral(resourceName: "green_add"), for: .normal)
            }
            cell.btnContact.tag = indexPath.row
            cell.btnContact.addTarget(self, action: #selector(clickOnRedGreenButton(sender:)), for: .touchUpInside)
            
        }else{
           cell.btnContact.setImage(#imageLiteral(resourceName: "red_cross"), for: .normal)
        }
        
        
        
            
        return cell
       
    }
    @objc func clickOnRedGreenButton(sender : UIButton)  {
        self.view.endEditing(true)
        if sender.image(for: .normal) == UIImage.init(named: "red_cross") {
            arrOptions.removeObject(at: sender.tag)
        }
        else {
            arrOptions.add("")
        }
        optionTableView.reloadData()
    }
}
