//
//  NotificationVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

    @IBOutlet weak var tblvw: UITableView!
    var scrollIndex = 0
    var arrNotification : NSMutableArray = []
    var isthereMoreData: Bool = false
    var refreshControl:UIRefreshControl!
    
    
    @IBOutlet weak var lblStatus : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblvw.isHidden = true
        lblStatus.isHidden = true
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblvw!.addSubview(refreshControl)
        
        self.tblvw.register(UITableViewCell.self, forCellReuseIdentifier: "cellNotification")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if timerTest != nil {
            timerTest?.invalidate()
            timerTest = nil
        }
        getAllComment(pooltoRefresh: true)
    }
    
    
    @objc func refresh(sender:AnyObject) {
         appdelegate.vibrateMobile()
        self.getAllComment(pooltoRefresh: true)
        
    }

    @IBAction func btnDelete(_ sender: Any) {
        if arrNotification.count > 0 {
            clearNotifications()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func clearNotifications() {
        
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!]
        
        WebHelper.requestUrlForPostWithType(GlobalConstant.clearNotificationUrl, strType: "Clear_Noti", dictParameter: dictParam,  controllerView: self, success: { (response) in
            
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                             self.tblvw.isHidden = true
                            self.arrNotification = []
                            self.tblvw.reloadData()
                        }
                        
                    } else {
                       
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    func getAllComment(pooltoRefresh:Bool) {
        
        if pooltoRefresh == true {
            
            scrollIndex = 0
        }
        
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"offset":String.init(format: "%d", scrollIndex as! Int)]
        
        WebHelper.requestUrlForPostWithType(GlobalConstant.notificationListUrl, strType: "bookmark_list", dictParameter: dictParam,  controllerView: self, success: { (response) in
            
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            
                            if pooltoRefresh == true {
                                
                                self.arrNotification = []
                                self.isthereMoreData = true
                            }
                            
                            let arr = response["Data"] as! NSArray
                            var intCount = 0
                            
                            for dict in arr {
                                
                                self.arrNotification.add(dict as! NSDictionary)
                                intCount += 1
                                
                                if intCount == 10 {
                                    
                                    intCount = 0
                                   // self.arrNotification.add(["post_type":3])
                                }
                            }
                            
                            if arr.count == 0 {
                                
                                
                                self.isthereMoreData = false
                            }
                            self.scrollIndex += 1
                            self.tblvw.reloadData()
                            self.tblvw.setContentOffset(.zero, animated: true)
                            self.refreshControl.endRefreshing()
                             self.tblvw.isHidden = false
                            self.lblStatus.isHidden = true
                        }

//                         DispatchQueue.main.async {
//                            self.arrNotification = (response["Data"] as! NSMutableArray)
//                            self.tblvw.reloadData()
//                        }
                    } else {
                        DispatchQueue.main.async {
                            
                            if self.arrNotification.count == 0{
                                self.tblvw.isHidden = true
                                self.lblStatus.isHidden = false
                            }
                            self.refreshControl.endRefreshing()
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
}



extension NotificationVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      //  let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellNotification", for: indexPath)
        
        let cellIdentifier = "cellNotification"
        var cell:UITableViewCell? =
            tblvw.dequeueReusableCell(withIdentifier: cellIdentifier)
        cell = UITableViewCell(style: UITableViewCellStyle.subtitle,reuseIdentifier: cellIdentifier)
        
        let dict = arrNotification[indexPath.row] as! NSDictionary
        
        //For notification message
        cell!.textLabel!.text = (dict["message"] as! String)
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.lineBreakMode = .byWordWrapping
        cell?.textLabel?.font = UIFont(name: "OpenSans-Regular", size: 15.0)
        
        //for notification created time
        let date = GlobalConstant.getDateFromString(strDate: dict["created_at"] as! String)
        cell!.detailTextLabel?.text = Date().offset(from: date)
        cell?.detailTextLabel?.font = UIFont(name: "OpenSans-Light", size: 13.0)
        cell?.detailTextLabel?.textColor = UIColor(red: 2.0/255.0, green: 148.0/255.0, blue: 214.0/255.0, alpha: 1)
        
        cell?.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == arrNotification.count-1 {
            if self.isthereMoreData == true {
                self.getAllComment(pooltoRefresh: false)
            }
        }
    }
}
