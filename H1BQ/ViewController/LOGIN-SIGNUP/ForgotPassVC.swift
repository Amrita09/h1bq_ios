//
//  ForgotPassVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {
    @IBOutlet var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func Submit(_ sender: Any) {
        if emailTextField.text != "" && GlobalConstant.isValidEmail(emailTextField.text!) == true {
            self.callAPI(strMessage: emailTextField.text!)
        }
        else{
          GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter a valid email id.", on: self)
        }
    }
    func callAPI(strMessage : String)  {
        let mainURL = "\(GlobalConstant.BaseURL)opt_reset"
        let dictParam = ["email":strMessage,"otp_reset_type":"2"]
        WebHelper.requestForReset_OTPPostUrl(mainURL, dictParameter: dictParam as NSDictionary, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] as! Int == 200 {
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                        vc.strEmail = self.emailTextField.text
                        vc.strOTPType = "Forgot"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
}
