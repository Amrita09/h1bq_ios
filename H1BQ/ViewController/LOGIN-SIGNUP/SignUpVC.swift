//
//  SignUpVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation

class SignUpVC: UIViewController,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    
    var strFirstName : String!
    var strLastName : String!
    var strEmail : String!
    var strSignupType : String = "1"
    var strGender : String = "M"
    var strCountry : String = ""
    var strZipcodeCountry : String = ""
    @IBOutlet var btnMale: UIButton!
    @IBOutlet var btnFemale: UIButton!
    var isTermsConditionsAccepted : Bool = false
    var isValidUsername : Bool = false
    @IBOutlet var clcView: UICollectionView!
    
    @IBOutlet var firstnameTextfield: UITextField!
    @IBOutlet var lastnameTextfield: UITextField!
    @IBOutlet var usernameTextfield: UITextField!
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var conEmailTextField: UITextField!
    @IBOutlet var zipcodeTextfield: UITextField!
    @IBOutlet var viewPassword: UIView!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var conPasswordTextfield: UITextField!
    @IBOutlet var viewConPAssword: UIView!
    var arrUsername : NSArray = []
    @IBOutlet var imgVerified : UIImageView!
    @IBOutlet var btnAlreadyAccount: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.GetValidUsername()
        if strEmail != nil {
            btnAlreadyAccount.isHidden = true
            viewPassword.isHidden = true
            viewConPAssword.isHidden = true
            strSignupType = "2"
            emailTextfield.text = strEmail
            conEmailTextField.text = strEmail
            firstnameTextfield.text = strFirstName
            lastnameTextfield.text = strLastName
        }
    }
    func GetValidUsername()  {
        let dictParam : NSDictionary = ["Username":""]
        WebHelper.requestForCheckUsernamePostUrl("\(GlobalConstant.BaseURL)get_username", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int {
                    DispatchQueue.main.async {
                        self.arrUsername = response["Data"] as! NSArray
                        self.clcView.reloadData()
                    }
                }
                else{
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didTappedonClickedGender(_ sender: UIButton) {
        if sender == btnMale {
            btnMale.setImage(#imageLiteral(resourceName: "radio_active"), for: .normal)
            btnFemale.setImage(#imageLiteral(resourceName: "radio_inactive"), for: .normal)
            strGender = "M"
        }
        else {
            btnFemale.setImage(#imageLiteral(resourceName: "radio_active"), for: .normal)
            btnMale.setImage(#imageLiteral(resourceName: "radio_inactive"), for: .normal)
            strGender = "F"
        }
    }
    @IBAction func didTappedonClickedTerms(_ sender: UIButton) {
        if isTermsConditionsAccepted == false {
            isTermsConditionsAccepted = true
            sender.setImage(#imageLiteral(resourceName: "termandcondtion"), for: .normal)
        }
        else {
            isTermsConditionsAccepted = false
            sender.setImage(#imageLiteral(resourceName: "tandc_empty"), for: .normal)
        }
    }
    @IBAction func countrybtnTapped(_ sender: UIButton) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Australia", "Canada", "Chile", "China", "India", "Mexico", "Philippines", "Singapore", "USA", "UK"]
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            self.strCountry = item
        }
        
        dropDown.show()
    }
    @IBAction func AlreadHaveAnAccount(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func SignUP(_ sender: Any) {
        
        if isvalidateAllfield() == true {
            
            let deviceToken = userDef.value(forKey: "fcmtoken") as! String
            let dictParam : [String:String] = ["first_name":firstnameTextfield.text!,"last_name":lastnameTextfield.text!,"username":usernameTextfield.text!,"email":emailTextfield.text!,"password":passwordTextfield.text!,"country":strCountry,"zipcode":zipcodeTextfield.text!,"sigup_type":strSignupType,"deviceid":deviceToken,"devicetype":"ios","gender":strGender]
            
            
            
            WebHelper.requestPostUrl("\(GlobalConstant.BaseURL)sigup_api", dictParameter: dictParam as NSDictionary, controllerView: self, success: { (response) in
                print(response)
                
                if response.count == 0 {
                    DispatchQueue.main.async {
                        
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                    }
                }
                else{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            if self.strEmail != nil {
                                UserDefaults.standard.set(true, forKey: "isLoginFromSocail")
                                UserDefaults.standard.set(String.init(format: "%d", response["User_id"] as! Int), forKey: "userid")
                                self.performSegue(withIdentifier: "signupToHome", sender:self)
                            }
                            else{
                                UserDefaults.standard.set(false, forKey: "isLoginFromSocail")
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                                vc.strEmail = self.emailTextfield.text
                                vc.strOTPType = "SignUp"
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                }
            })
        }
    }
    func isvalidateAllfield()-> Bool {
        if firstnameTextfield.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Firstname is required.", on: self)
            return false
        }
        if lastnameTextfield.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Lastname is required.", on: self)
            return false
        }
        if usernameTextfield.text == "" || self.isValidUsername == false {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Username is required.", on: self)
            return false
        }
        if (usernameTextfield.text?.count)! < 6 {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter minimum six characters!", on: self)
            return false
        }
        if emailTextfield.text == "" || GlobalConstant.isValidEmail(emailTextfield.text!) == false {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter a valid email id", on: self)
            return false
        }
        if conEmailTextField.text == "" || GlobalConstant.isValidEmail(conEmailTextField.text!) == false {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter a valid confirm email id", on: self)
            return false
        }
        if conEmailTextField.text != emailTextfield.text {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Email does not match.", on: self)
            return false
        }
        if strCountry == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Country is required.", on: self)
            return false
        }
        if zipcodeTextfield.text! == "" || 7 < (zipcodeTextfield.text?.count)! || 4 > (zipcodeTextfield.text?.count)! {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter a valid Zip code!", on: self)
            return false
        }
//        if strzipcode != strCountry {
//            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please fill valid zipcode!", on: self)
//            return false
//        }
        if self.strEmail == nil {
            if passwordTextfield.text == "" || (passwordTextfield.text?.count)! < 6 {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Password must be at least 6 characters.", on: self)
                return false
            }
            
            if conPasswordTextfield.text == "" {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please fill confirm password!", on: self)
                return false
            }
            if conPasswordTextfield.text != passwordTextfield.text {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Password does not matched!", on: self)
                return false
            }
        }
        
        if isTermsConditionsAccepted == false{
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please accept Terms & Conditions, Privacy Policy and Cookies Policy.", on: self)
            return false
        }
        return true
    }
//    func isValidateZipCodeForCountry(pincode : String)  {
//        let geocoder: CLGeocoder = CLGeocoder()
//        geocoder.geocodeAddressString(pincode, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) -> Void in
//            if placemarks != nil{
//                if ((placemarks?.count)! > 0) {
//                    let placemark: CLPlacemark = (placemarks?[0])!
//                    let country : String = placemark.country!
//                    let state: String = placemark.administrativeArea!
//                    print(state);
//                    self.strZipcodeCountry = country
//                }
//            }
//            else {
//                print(error?.localizedDescription)
//            }
//
//        } )
//    }
    @IBAction func TermsAndCondition(_ sender: UIButton) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        login.strTitle = "Terms & Conditions"
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func Privacy(_ sender: UIButton) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        login.strTitle = "Privacy Policy"
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func Coockies(_ sender: UIButton) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        login.strTitle = "Cookies Policy"
        self.navigationController?.pushViewController(login, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        if zipcodeTextfield == textField {
           // self.isValidateZipCodeForCountry(pincode: zipcodeTextfield.text!)
        }
        if usernameTextfield == textField {
            if usernameTextfield.text != "" {
                self.checkUsernameValid()
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == zipcodeTextfield {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    
    //MARK: - UITableViewDataSource,UITableViewDelegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrUsername.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        if indexPath.row != 2 {
            cell.lblName.text = "\(arrUsername.object(at: indexPath.row) as! String),"
        }
        else{
            cell.lblName.text = arrUsername.object(at: indexPath.row) as? String
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let font = UIFont(name: "OpenSans-Regular", size: 14)
        let fontAttributes = [NSAttributedStringKey.font: font]
        let myText = arrUsername.object(at: indexPath.row) as! String
        let size = myText.size(withAttributes: fontAttributes)
       
        return CGSize.init(width: size.width+10, height: 22)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        usernameTextfield.text = arrUsername.object(at: indexPath.row) as? String
        self.checkUsernameValid()
    }
    func checkUsernameValid()  {
         let dictParam : NSDictionary = ["Username":usernameTextfield.text!]
        WebHelper.requestForCheckUsernamePostUrl("\(GlobalConstant.BaseURL)check_username", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] as! Int == 200 {
                    DispatchQueue.main.async {
                        self.imgVerified.isHidden = false
                        self.imgVerified.image = #imageLiteral(resourceName: "verified")
                        self.isValidUsername = true
                    }
                }
                else{
                    DispatchQueue.main.async {
                        self.isValidUsername = false
                        self.imgVerified.isHidden = false
                        self.imgVerified.image = #imageLiteral(resourceName: "error")
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
}

