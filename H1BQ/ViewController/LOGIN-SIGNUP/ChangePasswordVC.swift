//
//  ChangePasswordVC.swift
//  H1BQ
//
//  Created by mac on 08/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    @IBOutlet var currenPassTextField: UITextField!
    @IBOutlet var newPasswordTextField: UITextField!
    @IBOutlet var confirmPassTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func didTappedonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTappedonUpdateBtn(_ sender: Any) {
        if isvalidateAllfield() == true {
            let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String,"old_password":currenPassTextField.text!,"new_password":newPasswordTextField.text!]
            WebHelper.requestForCheckUsernamePostUrl("\(GlobalConstant.BaseURL)password_change", dictParameter: dictParam, controllerView: self, success: { (response) in
                print(response)
                if response.count == 0 {
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                    }
                }
                else{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            self.currenPassTextField.text = ""
                            self.newPasswordTextField.text = ""
                            self.confirmPassTextField.text = ""
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                            
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                }
            })
        }
    }
    func isvalidateAllfield()-> Bool {
        if currenPassTextField.text == ""  {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Current password is required.", on: self)
            return false
        }
        if newPasswordTextField.text! == ""  {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "New password is required.", on: self)
            return false
        }
        if (newPasswordTextField.text?.count)! < 6 {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "New password must be at least 6 characters.", on: self)
            return false
        }
        if confirmPassTextField.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Confirm password is required.", on: self)
            return false
        }
        if (newPasswordTextField.text?.count)! < 6 {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Confirm password must be at least 6 characters.", on: self)
            return false
        }
        if newPasswordTextField.text != confirmPassTextField.text {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "The password does not match.", on: self)
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
