//
//  OTPVerifyVC.swift
//  H1BQ
//
//  Created by mac on 08/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class OTPVerifyVC: UIViewController {
    
    @IBOutlet var txtOTP1: UITextField!
    @IBOutlet var txtOTP2: UITextField!
    @IBOutlet var txtOTP3: UITextField!
    @IBOutlet var txtOTP4: UITextField!
    @IBOutlet var txtOTP5: UITextField!
    @IBOutlet var txtOTP6: UITextField!
    
    var strOTPType : String!
    var strEmail : String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        if strOTPType == "Forgot" {
//            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "We have sent you OTP. Please check your inbox and confirm your email id.", on: self)
        } else {
//            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please check the spam or junk mail folder in your email account linked to your H1BQ account", on: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func Submit(_ sender: Any) {
        
        if txtOTP1.text == "" || txtOTP2.text == "" || txtOTP3.text == "" || txtOTP4.text == "" || txtOTP5.text == "" || txtOTP6.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "OTP is required.", on: self)
            return
        }
        
        var dictParam : NSDictionary = ["email":strEmail!,"otp":String.init(format: "%@%@%@%@%@%@", txtOTP1.text!,txtOTP2.text!,txtOTP3.text!,txtOTP4.text!,txtOTP5.text!,txtOTP6.text!)]
        var mainURL = "\(GlobalConstant.BaseURL)verify_opt"
        if strOTPType == "Forgot" {
            mainURL = "\(GlobalConstant.BaseURL)password_opt_verify"
            dictParam = ["email":strEmail!,"password_otp":String.init(format: "%@%@%@%@%@%@", txtOTP1.text!,txtOTP2.text!,txtOTP3.text!,txtOTP4.text!,txtOTP5.text!,txtOTP6.text!)]
        }
        WebHelper.requestForReset_OTPPostUrl(mainURL, dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            
                            let data = response["Data"] as! NSDictionary
                            if self.strOTPType == "Forgot" {
                                UserDefaults.standard.set(data["userid"] as! String, forKey: "userid")
                                self.performSegue(withIdentifier: "otpToResetPass", sender: self)
                                
                            } else {
                                UserDefaults.standard.set(data["userid"] as! String, forKey: "userid")
                                self.performSegue(withIdentifier: "SignUpToHomeTab", sender: self)
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                } else {
                    
                    if response["Status"] as! String == "200" {
                        DispatchQueue.main.async {
                            let data = response["Data"] as! NSDictionary
                            if self.strOTPType == "Forgot" {
                                UserDefaults.standard.set(data["userid"] as! String, forKey: "userid")
                                self.performSegue(withIdentifier: "otpToResetPass", sender: self)
                                
                            } else {
                                UserDefaults.standard.set(data["userid"] as! String, forKey: "userid")
                                self.performSegue(withIdentifier: "SignUpToHomeTab", sender: self)
                            }
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    @IBAction func resendBtnTapped(_ sender: Any) {
        self.callAPI(strMessage: "OTP sent successfully.")
    }
    func callAPI(strMessage : String)  {
        var dictParam : NSDictionary = ["email":strEmail!,"otp_reset_type":"1"]
        var mainURL = "\(GlobalConstant.BaseURL)opt_reset"
        if strOTPType == "Forgot" {
            mainURL = "\(GlobalConstant.BaseURL)opt_reset"
            dictParam = ["email":strEmail!,"otp_reset_type":"2"]
        }
        
        WebHelper.requestForReset_OTPPostUrl(mainURL, dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] as! Int == 200 {
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: strMessage, on: self)
                    }
                }
                else{
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    
}
extension OTPVerifyVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if txtOTP1 == textField {
            txtOTP1.text = ""
            txtOTP2.text = ""
            txtOTP3.text = ""
            txtOTP4.text = ""
            txtOTP5.text = ""
            txtOTP6.text = ""
        }
        if txtOTP2 == textField {
            txtOTP2.text = ""
            txtOTP3.text = ""
            txtOTP4.text = ""
            txtOTP5.text = ""
            txtOTP6.text = ""
        }
        if txtOTP3 == textField {
            txtOTP3.text = ""
            txtOTP4.text = ""
            txtOTP5.text = ""
            txtOTP6.text = ""
        }
        if txtOTP4 == textField {
            txtOTP4.text = ""
            txtOTP5.text = ""
            txtOTP6.text = ""
        }
        if txtOTP5 == textField {
            txtOTP5.text = ""
            txtOTP6.text = ""
        }
        if txtOTP6 == textField {
            txtOTP6.text = ""
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if txtOTP1 == textField {
            if txtOTP1.text?.count == 1 {
                txtOTP1.resignFirstResponder()
                txtOTP2.becomeFirstResponder()
            }
            
        }
        if txtOTP2 == textField {
            if txtOTP2.text?.count == 1 {
                txtOTP2.resignFirstResponder()
                txtOTP3.becomeFirstResponder()
            }
        }
        if txtOTP3 == textField {
            if txtOTP3.text?.count == 1 {
                txtOTP3.resignFirstResponder()
                txtOTP4.becomeFirstResponder()
            }
        }
        if txtOTP4 == textField {
            if txtOTP4.text?.count == 1 {
                txtOTP4.resignFirstResponder()
                txtOTP5.becomeFirstResponder()
            }
        }
        if txtOTP5 == textField {
            if txtOTP5.text?.count == 1 {
                txtOTP5.resignFirstResponder()
                txtOTP6.becomeFirstResponder()
            }
        }
        if txtOTP6 == textField {
            if txtOTP6.text?.count == 1 {
                txtOTP6.resignFirstResponder()
            }
        }
        return true
    }
}
