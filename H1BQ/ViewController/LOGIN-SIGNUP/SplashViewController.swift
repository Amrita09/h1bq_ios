//
//  SplashViewController.swift
//  H1BQ
//
//  Created by mac on 21/08/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.value(forKey: "userid") != nil {
            
            self.performSegue(withIdentifier: "splashToHome", sender: self)
            
        } else {
            
            self.performSegue(withIdentifier: "splashToLogin", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
}
