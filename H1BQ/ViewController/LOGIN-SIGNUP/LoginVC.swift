//
//  LoginVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginVC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        emailTextField.text = "tarukhan.dbvertex@gmail.com"

        if UserDefaults.standard.value(forKey: "userid") != nil {
            
            self.performSegue(withIdentifier: "loginToHome", sender: self)
            
        } else {
            
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().signOut()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func btnSearch(_ sender: Any) {
    }
    
    
    
    @IBAction func SignUP(_ sender: Any) {
        
        let login = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func ForgotPass(_ sender: Any) {
        
        let login = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassVC") as! ForgotPassVC
        self.navigationController?.pushViewController(login, animated: true)
        
    }
    
    @IBAction func TapedONSignIN(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if isValidAllField() == true {
            
            let deviceToken = userDef.value(forKey: "fcmtoken") as! String
            let dictParam : NSDictionary = ["email":emailTextField.text!,"deviceid":deviceToken,"password":passwordTextField.text!, "devicetype":"ios"]
            
            WebHelper.requestForReset_OTPPostUrl("\(GlobalConstant.BaseURL)login_api", dictParameter: dictParam, controllerView: self, success: { (response) in
                
                print(response)
                
                if response.count == 0 {
                    
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                    }
                } else {
                    
                    if response["Data"] is String {
                        
                        if response["Data"] as! String == "OTP Not Verify" {
                            
                            DispatchQueue.main.async {
                                
                                UserDefaults.standard.set(false, forKey: "isLoginFromSocail")
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerifyVC") as! OTPVerifyVC
                                vc.strEmail = self.emailTextField.text
                                vc.strOTPType = "SignUp"
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        } else {
                            
                            DispatchQueue.main.async {
                                
                                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                            }
                        }
                    } else if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            
                            UserDefaults.standard.set(false, forKey: "isLoginFromSocail")
                            let data = response["Data"] as! NSDictionary
                            UserDefaults.standard.set(data["userid"] as! String, forKey: "userid")
                            self.performSegue(withIdentifier: "loginToHome", sender: self)
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                }
            })
        }
    }
    
    func isValidAllField() -> Bool {
        
        if emailTextField.text == "" || GlobalConstant.isValidEmail(emailTextField.text!) == false {
            
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please fill valid email address!", on: self)
            
            return false
        }
        
        if passwordTextField.text == "" {
            
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please fill password!", on: self)
            return false
        }
        return true
    }
    
    @IBAction func googleLoginBtnTapped(_ sender: Any) {
        
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        
        if let error = error {
            
            print("\(error.localizedDescription)")
            
        } else {
            
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            self.callAPIForSocailLogin(givenName: givenName!, familyName: familyName!, email: email!)
            // ...
        }
    }
    
    func callAPIForSocailLogin(givenName: String,familyName: String,email: String) {
        
        let dictParam : NSDictionary = ["email":email]
        
        WebHelper.requestForCheck_social_email("\(GlobalConstant.BaseURL)Check_social_email", dictParameter: dictParam, controllerView: self, success: { (response) in
            
            print(response)
            
            if response.count == 0 {
                
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] as! Int == 200 {
                    print(response)
                    DispatchQueue.main.async {
                        
                        UserDefaults.standard.set(true, forKey: "isLoginFromSocail")
                        UserDefaults.standard.set(response["User_id"] as! String, forKey: "userid")
                        self.performSegue(withIdentifier: "loginToHome", sender: self)
                    }
                } else {
                    
                    DispatchQueue.main.async {
                        
                        if response["Data"] as! String != "invalid login"{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                            vc.strFirstName = givenName
                            vc.strLastName = familyName
                            vc.strEmail = email
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        } else {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Invalid login!", on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
