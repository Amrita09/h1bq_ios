//
//  SettingVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {

    @IBOutlet var changePassVC: ShadowView!
    
    @IBOutlet weak var switchNotify : UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if UserDefaults.standard.bool(forKey: "isLoginFromSocail") == true {
            changePassVC.isHidden = true
        }
        
        switchNotify.addTarget(self, action: #selector(ckcikOnSwitchBtn(_:)), for: .valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func ckcikOnSwitchBtn(_ sender : UISwitch){
    
        if(sender.isOn == true){
            UIApplication.shared.registerForRemoteNotifications()
        }else{
            UIApplication.shared.unregisterForRemoteNotifications()
        }
        
    }
    

    @IBAction func Show_Profile(_ sender: Any) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func Favorites(_ sender: Any) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "FavoritesVC") as! FavoritesVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func ContactUs(_ sender: Any) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func TermsAndCondition(_ sender: UIButton) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        login.strTitle = "Terms & Conditions"
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func Privacy(_ sender: UIButton) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        login.strTitle = "Privacy Policy"
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func changePassword(_ sender: UIButton) {
        let login = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBAction func logout(_ sender: UIButton) {
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: GlobalConstant.AppName, message: "Are you sure you want to Log out?", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Yes", style: .cancel) { action -> Void in
            //Do some stuff
            UserDefaults.standard.removeObject(forKey: "userid")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let navigate = UINavigationController.init(rootViewController: vc)
            navigate.isNavigationBarHidden = true
            appdelegate.window?.rootViewController = navigate
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "No", style: .default) { action -> Void in
            //Do some other stuff
        }
        actionSheetController.addAction(nextAction)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
