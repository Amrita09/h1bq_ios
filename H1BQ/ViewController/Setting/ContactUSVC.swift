//
//  ContactUSVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import DropDown
class ContactUSVC: UIViewController {

    @IBOutlet var fullnameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    var strCode : String = "+1"
    var strRegarding : String = ""
    @IBOutlet var contactTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var btnRegarding: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getUserProfile()
    }
    func getUserProfile()  {
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String]
        WebHelper.requestForCheckUsernamePostUrl("\(GlobalConstant.BaseURL)my_profile", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] as! Int == 200 {
                    DispatchQueue.main.async {
                        let data = response["Data"] as! NSDictionary
                        self.fullnameTextField.text = String.init(format: "%@ %@", data["first_name"] as! String,data["last_name"] as! String)
                        self.emailTextField.text = data["Email"] as? String
                        
                    }
                }
                else{
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func countryCodebtnTapped(_ sender: UIButton) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["+61", "+1", "+56","+86", "+91", "+52","+63","+65","+44"]
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.strCode = item
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
        }
        
        dropDown.show()
    }
    @IBAction func regardingbtnTapped(_ sender: UIButton) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Account", "Marketing", "Report bug"]
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.strRegarding = item
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
        }
        
        dropDown.show()
    }

    @IBAction func Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func didTappedonSubmit(_ sender: Any) {
        if isvalidateAllfield() == true {
            let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String,"full_name":fullnameTextField.text!,"email":emailTextField.text!,"regarding":strRegarding,"descripation":messageTextView.text!,"mobile":String.init(format: "%@%@", strCode,contactTextField.text!)]
            WebHelper.requestForCheckUsernamePostUrl("\(GlobalConstant.ContactURL)add_contact", dictParameter: dictParam, controllerView: self, success: { (response) in
                print(response)
                if response.count == 0 {
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                    }
                }
                else{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            self.contactTextField.text = ""
                            self.strRegarding = ""
                            self.messageTextView.text = ""
                            self.btnRegarding.setTitle("Regarding", for: .normal)
                            self.btnRegarding.setTitleColor(UIColor.lightGray, for: .normal)
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Thanks you for contacting us!", on: self)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                }
            })
        }
    }
    func isvalidateAllfield()-> Bool {
        if strCode == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Country code is required.", on: self)
            return false
        }
        if contactTextField.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Contact number is required.", on: self)
            return false
        }
        if (contactTextField.text?.count)! < 10 || (contactTextField.text?.count)! > 12 {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please fill valid contact number!", on: self)
            return false
        }
        if strRegarding == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Regarding option is required.", on: self)
            return false
        }
        if messageTextView.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Message is required.", on: self)
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
