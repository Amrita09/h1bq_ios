//
//  FavoritesVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import AlamofireImage
import DropDown
import AVFoundation

class FavoritesVC: UIViewController {
    var scrollIndex = 0
    var arrPostList : NSMutableArray = []
    var isthereMoreData: Bool = false
    var arrOption : [String] = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    @IBOutlet var tblViewList: UITableView!
    var refreshControl:UIRefreshControl!
    @IBOutlet var reportToAbuseViuew: UIView!
    @IBOutlet var abuseTextview: UITextView!
    var dictDetail: NSDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblViewList!.addSubview(refreshControl)
    }
    @objc func refresh(sender:AnyObject)
    {
        //DO
         appdelegate.vibrateMobile()
        self.getPostByBucketID(pooltoRefresh: true)
        
    }
    @IBAction func didTappedonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.arrPostList = []
        self.getPostByBucketID(pooltoRefresh: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if timerTest != nil {
            timerTest?.invalidate()
            timerTest = nil
        }
    }
   
    func getPostByBucketID(pooltoRefresh:Bool)  {
        var count = self.arrPostList.count
        if pooltoRefresh == true {
            count = 0
        }
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"offset":"\(count)"]
        
        
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)bookmark_list", strType: "bookmark_list", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            if pooltoRefresh == true {
                                self.arrPostList = []
                                self.isthereMoreData = true
                            }
                            let arr = response["Data"] as! NSArray
                            for dict in arr {
                                self.arrPostList.add(dict as! NSDictionary)
                            }
                            if arr.count == 0 {
                                self.isthereMoreData = false
                            }
                            self.scrollIndex = 0
                            self.tblViewList.reloadData()
                            self.tblViewList.contentOffset = CGPoint.init(x: 0, y: 0)
                            self.refreshControl.endRefreshing()
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.reportToAbuseViuew.isHidden = true
    }
    @IBAction func okButttonClicked(_ sender: Any) {
        if abuseTextview.text == "" || abuseTextview.text == "Write here..." {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter report", on: self)
            return
        }
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"message":abuseTextview.text]
        
        
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)report_abous", strType: "report_abous", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            self.reportToAbuseViuew.isHidden = true
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
}

extension FavoritesVC : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPostList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dict  = self.arrPostList[indexPath.row] as! NSDictionary
        print(dict)
        if dict["post_type"] as! Int == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! TableViewCell
            if dict["bookmark_status"] as! Int == 0 {
                cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
            } else {
                cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
            }
            if dict["view_status"] as! Int == 0 {
                cell.btnViewAll.setImage(#imageLiteral(resourceName: "viewer"), for: .normal)
            } else {
                cell.btnViewAll.setImage(#imageLiteral(resourceName: "viewer_blue"), for: .normal)
            }
            cell.btnBookmark.tag = indexPath.row
            cell.btnBookmark.addTarget(self, action: #selector(bookmarkButtonClicked(sender:)), for: .touchUpInside)
            cell.btnMore.tag = indexPath.row
            cell.btnMore.addTarget(self, action: #selector(moreButtonClicked(sender:)), for: .touchUpInside)
            cell.lblName.text = dict["username"] as? String ?? ""
            cell.lblTitle.text = dict["title"] as? String ?? ""
            
            
            let decodeText = dict["dicription"] as? String
            let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
            if let decoded = msg.fromBase64() {
                cell.lblDescription.text = decoded
            }else{
                cell.lblDescription.text = dict["dicription"] as? String
            }
            
            if(cell.lblDescription.text.count > 55){
                cell.lblDescription.textContainer.maximumNumberOfLines = 2
                cell.lblDescription.textContainer.lineBreakMode = .byTruncatingTail
                cell.lblSeeMore.text = "See More"
            }else{
                cell.lblDescription.textContainer.lineBreakMode = .byWordWrapping
                cell.lblSeeMore.text = ""
            }
            
            cell.imgBG.image = nil
            cell.imgBG.af_cancelImageRequest()
            if dict["post_image"] as! String != "" {
                cell.imgBG.af_setImage(withURL: URL.init(string: dict["post_image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
            }
            let date = GlobalConstant.getDateFromString(strDate: dict["add_date"] as! String)
            print(Date().offset(from: date))
            cell.lblDateTime.text = Date().offset(from: date)
            cell.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if dict["like_status"] as! Int == 1 {
                cell.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            }
            cell.btnLike.setTitle(String.init(format: "%d", dict["count_like"] as! Int), for: .normal)
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(likeButtonClicked(sender:)), for: .touchUpInside)
            cell.btnComment.tag = indexPath.row
            cell.btnComment.setTitle(String.init(format: "%d", dict["count_comment"] as! Int), for: .normal)
            cell.btnComment.addTarget(self, action: #selector(commentButtonClicked(sender:)), for: .touchUpInside)
            cell.btnViewAll.setTitle(String.init(format: "%d", dict["count_view"] as! Int), for: .normal)
            cell.btnContact.tag = indexPath.row
            cell.btnContact.addTarget(self, action: #selector(contactButtonClicked(sender:)), for: .touchUpInside)
           // cell.lblSeeMore.text = ""
            

            
//            if cell.lblDescription.heightForTextView(text: cell.lblDescription.text!, font: UIFont.init(name: "OpenSans-Regular", size: 9)!, width: cell.lblDescription.frame.size.width) > 25 {
//                cell.lblSeeMore.text = "See More"
//            }
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(shareButtonClicked(sender:)), for: .touchUpInside)
            
            if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
                cell.btnLike.isEnabled = false
                cell.btnComment.isEnabled = false
                cell.btnShare.isEnabled = false
                cell.btnBookmark.isEnabled = false
                cell.btnMore.isEnabled = false
                
                cell.btnLike.alpha = 0.5
                cell.btnComment.alpha = 0.5
                cell.btnShare.alpha = 0.5
                cell.btnBookmark.alpha = 0.5
                cell.btnMore.alpha = 0.5
            }
            
            
            return cell
        }
        else if dict["post_type"] as! Int == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PollCell", for: indexPath) as! TableViewCell
            if dict["bookmark_status"] as! Int == 0 {
                cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
            } else {
                cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
            }
            cell.btnBookmark.tag = indexPath.row
            cell.btnBookmark.addTarget(self, action: #selector(bookmarkButtonClicked(sender:)), for: .touchUpInside)
            cell.btnMore.tag = indexPath.row
            cell.btnMore.addTarget(self, action: #selector(moreButtonClicked(sender:)), for: .touchUpInside)
            cell.lblName.text = dict["username"] as? String ?? ""
            cell.lblTitle.text = dict["title"] as? String ?? ""
            
            
            let decodeText = dict["dicription"] as? String
            let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
            if let decoded = msg.fromBase64() {
                cell.lblDescription.text = decoded
            }else{
                cell.lblDescription.text = dict["dicription"] as? String
            }
            
            
            //cell.txtDescription.text = dict["dicription"] as? String ?? ""
            cell.lblVoteCount.text = String.init(format: "\t\t %d Votes", dict["count_vote"] as! Int)
            let date = GlobalConstant.getDateFromString(strDate: dict["add_date"] as! String)
            cell.lblDateTime.text = Date().offset(from: date)
            cell.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if dict["like_status"] as! Int == 1 {
                cell.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            }
            cell.btnLike.setTitle(String.init(format: "%d", dict["count_like"] as! Int), for: .normal)
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(likeButtonClicked(sender:)), for: .touchUpInside)
            cell.btnComment.tag = indexPath.row
            cell.btnComment.setTitle(String.init(format: "%d", dict["count_comment"] as! Int), for: .normal)
            cell.btnComment.addTarget(self, action: #selector(commentButtonClicked(sender:)), for: .touchUpInside)
            cell.btnViewAll.setTitle(String.init(format: "%d", dict["count_view"] as! Int), for: .normal)
            cell.btnContact.tag = indexPath.row
            cell.btnContact.addTarget(self, action: #selector(contactButtonClicked(sender:)), for: .touchUpInside)
            cell.btnShare.tag = indexPath.row
            cell.btnShare.addTarget(self, action: #selector(shareButtonClicked(sender:)), for: .touchUpInside)
            
            if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
                cell.btnLike.isEnabled = false
                cell.btnComment.isEnabled = false
                cell.btnShare.isEnabled = false
                cell.btnBookmark.isEnabled = false
                cell.btnMore.isEnabled = false
                
                cell.btnLike.alpha = 0.5
                cell.btnComment.alpha = 0.5
                cell.btnShare.alpha = 0.5
                cell.btnBookmark.alpha = 0.5
                cell.btnMore.alpha = 0.5
            }
            
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCell", for: indexPath) as! TableViewCell
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(indexPath.row)
        print(arrPostList.count-1)
        if indexPath.row == arrPostList.count-1 {
            if self.isthereMoreData == true {
                self.getPostByBucketID(pooltoRefresh: false)
            }
        }
    }
    @objc func contactButtonClicked(sender: UIButton)  {
        let dict  = self.arrPostList[sender.tag] as! NSDictionary
        if dict["post_type"] as! Int == 1 {
            let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
            self.navigationController?.pushViewController(polldetail, animated: true)
        }
        if dict["post_type"] as! Int == 2 {
            let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PollDetailViewController") as! PollDetailViewController
            polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
            self.navigationController?.pushViewController(polldetail, animated: true)
        }
    }
    @objc func bookmarkButtonClicked(sender: UIButton)  {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
        let dict  = self.arrPostList[sender.tag] as! NSDictionary
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dict["post_id"] as! Int)]
         appdelegate.vibrateMobile()
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)Add_bookmark", strType: "Add_bookmark", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            if response["Data"] as! String == "Like"{
                                sender.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
                            }
                            else
                            {
                                sender.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                            }
                            self.getPostByBucketID(pooltoRefresh: true)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
        }
    }
    @objc func moreButtonClicked(sender: UIButton)  {
        let dict  = self.arrPostList[sender.tag] as! NSDictionary
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        if Int(UserDefaults.standard.value(forKey: "userid") as! String) == dict["user_id"] as? Int {
            if dict["post_type"] as! Int == 1 {
                // The list of items to display. Can be changed dynamically
                dropDown.dataSource = ["Delete Post", "Edit Post"]
            }
            else{
                // The list of items to display. Can be changed dynamically
                dropDown.dataSource = ["Delete Post"]
            }
        }
        else{
            dropDown.dataSource = ["Report Abuse"]
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if index == 0 {
                if item == "Report Abuse" {
                    if dict["report_abuse_status"] as! Int == 0 {
                        self.reportAbuseAPICalled(dictPost: dict)
                    }
                    else{
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "You have already report for this post.", on: self)
                    }
                }
                else{
                    if Int(UserDefaults.standard.value(forKey: "userid") as! String) == dict["user_id"] as? Int {
                        self.deletePostAlert(dictPost: dict)
                    }
                }
            }
            else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
                vc.dictPost = dict
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        dropDown.show()
    }
    func reportAbuseAPICalled(dictPost:NSDictionary) {
        dictDetail = dictPost
        self.reportToAbuseViuew.isHidden = false
    }
    func deletePostAlert(dictPost:NSDictionary) {
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: GlobalConstant.AppName, message: "Are you sure you want to delete this post?", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Yes", style: .cancel) { action -> Void in
            //Do some stuff
            self.deletePostApiCalled(dict: dictPost)
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "No", style: .default) { action -> Void in
            //Do some other stuff
        }
        actionSheetController.addAction(nextAction)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func deletePostApiCalled(dict :NSDictionary) {
        let dictParam : NSDictionary = ["post_id":String.init(format: "%d", dict["post_id"] as! Int)]
        
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)delete_post", strType: "delete_post", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            self.arrPostList.remove(dict)
                            self.tblViewList.reloadData()
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    @objc func likeButtonClicked(sender: UIButton)  {
        
        let count = Int(sender.title(for: .normal)!)
        if (sender.currentImage == #imageLiteral(resourceName: "like_Blue")){
            sender.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if count != 0 {
                sender.setTitle("\(count!-1)", for: .normal)
            }
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            sender.setTitle("\(count!+1)", for: .normal)
        }
        
        let dict  = self.arrPostList[sender.tag] as! NSDictionary
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dict["post_id"] as! Int)]
         appdelegate.vibrateMobile()
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)like_post", strType: "like_post", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            if response["Data"] as! String == "Like"{
                                
                            }
                            else
                            {
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    @objc func shareButtonClicked(sender: UIButton)  {
        let dict  = self.arrPostList[sender.tag] as! NSDictionary
        let text = dict["title"] as! String
        let myWebsite = NSURL(string:dict["post_image"] as! String)
        let shareAll = [text ,myWebsite ?? ""] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func commentButtonClicked(sender: UIButton)  {
        
    }
}
extension FavoritesVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write here..." {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Write here..."
            textView.textColor = UIColor.lightGray
        }
    }
}

