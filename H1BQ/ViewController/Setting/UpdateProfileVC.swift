//
//  UpdateProfileVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import DropDown

class UpdateProfileVC: UIViewController,UITextFieldDelegate {
    var strCountry : String = ""
    @IBOutlet var firstnameTextfield: UITextField!
    @IBOutlet var lastnameTextfield: UITextField!
    @IBOutlet var usernameTextfield: UITextField!
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var btnCountry: UIButton!
    @IBOutlet var zipcodeTextfield: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var lastView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var zipView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
            firstnameTextfield.isUserInteractionEnabled = false
            lastnameTextfield.isUserInteractionEnabled = false
            usernameTextfield.isUserInteractionEnabled = false
            emailTextfield.isUserInteractionEnabled = false
            btnCountry.isUserInteractionEnabled = false
            zipcodeTextfield.isUserInteractionEnabled = false
          
            firstView.backgroundColor = UIColor(red: 238.0/255.0, green:238.0/255.0, blue: 238.0/255.0, alpha: 1)
            lastView.backgroundColor = UIColor(red: 238.0/255.0, green:238.0/255.0, blue: 238.0/255.0, alpha: 1)
            countryView.backgroundColor = UIColor(red: 238.0/255.0, green:238.0/255.0, blue: 238.0/255.0, alpha: 1)
            zipView.backgroundColor = UIColor(red: 238.0/255.0, green:238.0/255.0, blue: 238.0/255.0, alpha: 1)
            
            btnUpdate.isUserInteractionEnabled = false
            btnUpdate.alpha = 0.5
            
        }
        // Do any additional setup after loading the view.
        self.getUserProfile()
    }
    func getUserProfile()  {
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String]
        WebHelper.requestForCheckUsernamePostUrl("\(GlobalConstant.BaseURL)my_profile", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
            }
            else{
                if response["Status"] as! Int == 200 {
                    DispatchQueue.main.async {
                        let data = response["Data"] as! NSDictionary
                        self.firstnameTextfield.text = data["first_name"] as? String
                        self.lastnameTextfield.text = data["last_name"] as? String
                        self.usernameTextfield.text = data["username"] as? String
                        self.emailTextfield.text = data["Email"] as? String
                        self.strCountry = data["Country"] as! String
                        self.btnCountry.setTitle(self.strCountry, for: .normal)
                        self.zipcodeTextfield.text = data["Cuntray_Zipe_Code"] as? String
                    }
                }
                else{
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                    }
                }
            }
        }, failure: { (error) in
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func countrybtnTapped(_ sender: UIButton) {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Australia", "Canada", "Chile", "China", "India", "Mexico", "Philippines", "Singapore", "USA", "UK"]
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor.black, for: .normal)
            self.strCountry = item
        }
        
        dropDown.show()
    }
    @IBAction func Update(_ sender: Any) {
        if isvalidateAllfield() == true {
            let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String,"first_name":firstnameTextfield.text!,"last_name":lastnameTextfield.text!,"email":emailTextfield.text!,"country":self.strCountry,"zipcode":zipcodeTextfield.text!]
            
            WebHelper.requestForUpdateProfilePostUrl("\(GlobalConstant.BaseURL)update_profile", dictParameter: dictParam, controllerView: self, success: { (response) in
                print(response)
                if response.count == 0 {
                    DispatchQueue.main.async {
                        
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                    }
                }
                else{
                    if response["Status"] is Int {
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Profile successfully updated!", on: self)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                }
            })
        }
    }
    func isvalidateAllfield()-> Bool {
        if firstnameTextfield.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Firstname is required.", on: self)
            return false
        }
        if lastnameTextfield.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Lastname is required.", on: self)
            return false
        }
        if strCountry == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Country is required.", on: self)
            return false
        }
        if zipcodeTextfield.text! == "" || 7 < (zipcodeTextfield.text?.count)! || 4 > (zipcodeTextfield.text?.count)! {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter a valid Zip code! ", on: self)
            return false
        }
        //        if strzipcode != strCountry {
        //            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please fill valid zipcode!", on: self)
        //            return false
        //        }

        return true
    }
    //MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == zipcodeTextfield {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
