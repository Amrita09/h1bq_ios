//
//  ResetPassVC.swift
//  H1BQ
//
//  Created by mac on 11/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class ResetPassVC: UIViewController {
    
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var conPasswordTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //resetToTabbar
    }
    @IBAction func didTappedonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func didTappedonSubmit(_ sender: Any) {
        if passwordTextfield.text == ""  {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Password is required.", on: self)
            return
        }
        if  conPasswordTextfield.text == "" {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Confirm password is required.", on: self)
            return
        }
        if (passwordTextfield.text?.count)! < 6 {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Password must be at least 6 characters.", on: self)
            return 
        }
        if (conPasswordTextfield.text?.count)! < 6  {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Confirm password must be at least 6 characters.", on: self)
            return
        }
        if passwordTextfield.text != conPasswordTextfield.text {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "The password does not match.", on: self)
            return
        }
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String,"new_password":passwordTextfield.text!]
        let mainURL = "\(GlobalConstant.BaseURL)forget_password_change"
        
        WebHelper.requestForReset_OTPPostUrl(mainURL, dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] as! Int == 200 {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "resetToTabbar", sender: self)
                    }
                }
                else{
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
