//
//  SearchVC.swift
//  H1BQ
//
//  Created by mac on 06/07/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {
    
    @IBOutlet weak var tblSearchList: UITableView!
    @IBOutlet weak var searchBarField: UISearchBar!
    @IBOutlet weak var lblstatus : UILabel!
    
    var arrSearchResult: NSArray = []
    var arrHeader = ["H1B","PERM","I-140","I-131(AP)","I-485","I-765(EAD)","MISC"]
    var searchStringArray = [String]()
    var isSearch = false
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tblSearchList.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if timerTest != nil {
            timerTest?.invalidate()
            timerTest = nil
        }
        isSearch = false
        self.searchBarField.text = ""
        self.tblSearchList.isHidden = true
        self.lblstatus.isHidden = true
        //self.tblSearchList.reloadData()
    }
    
    @IBAction func btnSearch(_ sender: Any) {
        self.searchBarField.resignFirstResponder()
        let trimmedString = searchBarField.text!.trimmingCharacters(in: .whitespaces)
        if trimmedString.count > 0 {
            searchStringArray.append(trimmedString)
            UserDefaults.standard.set(searchStringArray, forKey: "searchArry")
            callAPIforGetSearchResult(searchText: trimmedString)
        }
    }
    
}

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        
        if isSearch == false {
            if let string = arrSearchResult[indexPath.row] as? String{
                cell.lblTitle.text =  string
                cell.btnContact.isHidden = true
            }
        }else{
            let dict = arrSearchResult.object(at: indexPath.row) as! NSDictionary
            cell.lblTitle.text = dict["title"] as? String ?? ""
            cell.btnContact.setTitle("\(arrHeader[(dict["bucket_id"] as! Int)-1])", for: .normal)
            cell.btnContact.isHidden = false
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.searchBarField.resignFirstResponder()
        if isSearch == false {
            let string = arrSearchResult[indexPath.row]
            searchBarField.text = string as! String
            callAPIforGetSearchResult(searchText: string as! String)
            
        }else{
            let dict  = self.arrSearchResult[indexPath.row] as! NSDictionary
            
            if dict["post_type"] as! Int == 1 {
                
                let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
                self.navigationController?.pushViewController(polldetail, animated: true)
                
            } else if dict["post_type"] as! Int == 2 {
                
                let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PollDetailViewController") as! PollDetailViewController
                polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
                self.navigationController?.pushViewController(polldetail, animated: true)
            }
            
            self.arrSearchResult = []
            searchBarField.text = ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
}

extension SearchVC: UISearchBarDelegate {
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchLastString()
        
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.tblSearchList.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0{
            searchLastString()
        }
    }
    
    func searchLastString() -> Void {
        isSearch = false
        searchStringArray = UserDefaults.standard.value(forKey: "searchArry") as? [String] ?? []
        if searchStringArray.count > 0{
            
            arrSearchResult = searchStringArray.suffix(5).reversed() as NSArray
            self.tblSearchList.isHidden = false
            self.lblstatus.isHidden = true
            self.lblstatus.text = "NO SUGGESTION IN DROPDOWN"
            tblSearchList.reloadData()
        }else{
            self.tblSearchList.isHidden = true
            self.lblstatus.isHidden = false
        }
    }
    
    func callAPIforGetSearchResult(searchText: String) {
        isSearch = true
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.SearchURL)search", strType: "search", dictParameter: ["get_txt":searchText], controllerView: self, success: { (response) in
            
            print(response)
            
            if response.count == 0 {
                
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    print( response["Status"] is Int)
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            self.arrSearchResult = response["Data"] as! NSArray
                            if(self.arrSearchResult.count == 0){
                                self.tblSearchList.isHidden = true
                                self.lblstatus.isHidden = false
                                self.lblstatus.text = "NO SEARCH RESULT FOUND"
                                
                            }else{
                                self.tblSearchList.isHidden = false
                                self.lblstatus.isHidden = true
                                self.tblSearchList.reloadData()
                            }
                            
                            
                        }
                    } else {
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
}

