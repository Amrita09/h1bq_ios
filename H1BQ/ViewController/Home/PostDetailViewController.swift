//
//  PollDetailViewController.swift
//  H1BQ
//
//  Created by mac on 01/08/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit
import DropDown
import Alamofire

class PostDetailViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate ,UIGestureRecognizerDelegate {
    var imagePicker = UIImagePickerController()
    var strPostId: String!
    var dictDetail:NSDictionary!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var btnBookmark: UIButton!
    @IBOutlet var constHeightCollection: NSLayoutConstraint!
    @IBOutlet var imgBG: UIImageView!
    // Profile TimeLine
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var btnComment: UIButton!
    @IBOutlet var lblDescription: UITextView!
    @IBOutlet var btnViewAll: UIButton!
    var arrAnswer : NSArray = []
    var arrOption : [String] = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var strCommentType:String! = "1"
    var strCommentID :String! = ""
    var nested_comment_id :String! = ""
    var postOwnerID : Int!
    @IBOutlet var tblComment: UITableView!
    var arrComment : NSMutableArray = []
    var isthereMoreData: Bool = true
    @IBOutlet var txtComment: UITextField!
    @IBOutlet var lblCommentCount: UILabel!
    
    @IBOutlet var reportToAbuseViuew: UIView!
    @IBOutlet var abuseTextview: UITextView!
    @IBOutlet var zoomView: UIView!
    @IBOutlet var imgViewZoom: UIImageView!
    var imageData:Data?
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnSent: UIButton!
    @IBOutlet weak var btnAttachment: UIButton!
    @IBOutlet weak var txtViewHeight : NSLayoutConstraint!
    
    
    var postIndex = 0
    var strBucketID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblComment.tableFooterView = UIView()
        
        if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
            self.btnLike.isEnabled = false
            self.btnComment.isEnabled = false
            self.btnShare.isEnabled = false
            self.btnBookmark.isEnabled = false
            self.btnMore.isEnabled = false
            self.btnAttachment.isEnabled = false
            self.btnSent.isEnabled = false
            self.txtComment.isEnabled = false
            
            self.btnLike.alpha = 0.5
            self.btnComment.alpha = 0.5
            self.btnShare.alpha = 0.5
            self.btnBookmark.alpha = 0.5
            self.btnMore.alpha = 0.5
            self.btnAttachment.alpha = 0.5
            self.btnSent.alpha = 0.5
            
        }
        
        
        // Do any additional setup after loading the view.
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if timerTest != nil {
            timerTest?.invalidate()
            timerTest = nil
        }
        self.arrComment = []
        
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
            self.getPostOrPollDetails()
        }else{
            
            let allItems = DBManager.sharedInstance.getDataFromDB()
            let kkk = allItems.filter("bucket == \(strBucketID)", self)
            let item = kkk[postIndex]
            print(item)
            
            if item.bookmark_status == 0 {
                btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
            } else {
                btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
            }
            lblUsername.text = item.username
            lblTitle.text = item.title
            
            let decodeText = item.dicription.description
            let msg = String(describing: decodeText.filter { !" \n\t\r".contains($0) })
            
            if let decoded = msg.fromBase64() {
                lblDescription.text = decoded
            }else{
                lblDescription.text = item.dicription.description
            }
            //self.txtViewHeight.constant = lblDescription.contentSize.height
            
            
            postOwnerID = item.user_id
            
            let postImg = item.post_image
            if postImg != "" {
                imgBG.af_setImage(withURL: URL.init(string: postImg)!, placeholderImage:UIImage.init(named: "image.jpg"))
                imgViewZoom.af_setImage(withURL: URL.init(string: postImg)!, placeholderImage:UIImage.init(named: "image.jpg"))
            }
           // self.arrAnswer = dict["answer"] as? NSArray ?? []
            
            let date = GlobalConstant.getDateFromString(strDate: item.add_date)
            lblDateTime.text = String(Date().offset(from: date))
            btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if item.like_status == 1 {
                btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            }
            btnLike.setTitle(String.init(format: "%d", item.count_like), for: .normal)
            btnComment.setTitle(String.init(format: "%d", item.count_comment), for: .normal)
            btnViewAll.setTitle(String.init(format: "%d",item.count_view), for: .normal)
        }
        
        
        
    }
    
    func getPostOrPollDetails() {
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":strPostId]
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)poll_detail", strType: "poll_detail", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            DispatchQueue.main.async {
                self.getAllComment()
                self.dictDetail = response["Data"] as! NSDictionary
                self.setPostDetailOnPollDetails(dict: response["Data"] as! NSDictionary)
            }
        }) { (error) in
            print(error!)
        }
    }
    func getAllComment() {
        
       
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":strPostId,"offset":"0"]
        
        WebHelper.requestForReset_OTPPostUrl("\(GlobalConstant.CommentURL)load_comment", dictParameter: dictParam, controllerView: self, success: { (response) in
            
            print(response)
            
            if response.count == 0 {
                
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            self.arrComment.removeAllObjects()
                            let arr = response["Data"] as! NSArray
                            for dict in arr {
                                self.arrComment.add(dict as! NSDictionary)
                            }
                            if arr.count == 0 {
                                self.isthereMoreData = false
                            }
                            
                            if self.arrComment.count != 0 {
                                self.lblCommentCount.text = String.init(format: "(%d+)", self.arrComment.count)
                                self.btnComment.setTitle(String.init(format: "%d", self.arrComment.count), for: .normal)
                                self.tblComment.reloadData()
                            }
                            
                            self.constHeightCollection.constant = self.tblComment.contentSize.height+100
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setPostDetailOnPollDetails(dict : NSDictionary) {
        if dict["bookmark_status"] as! Int == 0 {
            btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
        } else {
            btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
        }
        lblUsername.text = dict["username"] as? String ?? ""
        lblTitle.text = dict["title"] as? String ?? ""
        
        let decodeText = dict["dicription"] as? String
        let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
        
        if let decoded = msg.fromBase64() {
                lblDescription.text = decoded
            }else{
                lblDescription.text = dict["dicription"] as? String
            }
        //self.txtViewHeight.constant = lblDescription.contentSize.height

        
        
        postOwnerID = dict["user_id"] as! Int
        if dict["post_image"] as! String != "" {
            imgBG.af_setImage(withURL: URL.init(string: dict["post_image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
            imgViewZoom.af_setImage(withURL: URL.init(string: dict["post_image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
        }
        self.arrAnswer = dict["answer"] as? NSArray ?? []

        let date = GlobalConstant.getDateFromString(strDate: dict["add_date"] as! String)
        lblDateTime.text = String(Date().offset(from: date))
        btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
        if dict["like_status"] as! Int == 1 {
            btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
        }
        btnLike.setTitle(String.init(format: "%d", dict["count_like"] as! Int), for: .normal)
        btnComment.setTitle(String.init(format: "%d", dict["count_comment"] as! Int), for: .normal)
        btnViewAll.setTitle(String.init(format: "%d", dict["count_view"] as! Int), for: .normal)
    }
    @IBAction func Back(_ sender: Any) {
        isBack = "true"
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func imageViewClicked(_ sender: UIGestureRecognizer) {
        
        
       
        
        if (sender.view as? UIImageView)?.tag == 200 {
            if let image = (sender.view as? UIImageView)?.image {
                 zoomView.isHidden = false
                    imgViewZoom.image = image
                }
            
         } else {
             zoomView.isHidden = false
            imgViewZoom.af_setImage(withURL: URL.init(string: dictDetail["post_image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
        }
    }
    
    @IBAction func hideButtonclickedonZoomView(_ sender: UIButton) {
        zoomView.isHidden = true
    }
    
    
    @IBAction func bookmarkButtonClicked(sender: UIButton)  {
        
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", self.dictDetail["post_id"] as! Int)]
                appdelegate.vibrateMobile()
                WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)Add_bookmark", strType: "Add_bookmark", dictParameter: dictParam, controllerView: self, success: { (response) in
                    print(response)
                    if response.count == 0 {
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                        }
                    }
                    else{
                        if response["Status"] is Int{
                            if response["Status"] as! Int == 200 {
                                DispatchQueue.main.async {
                                    if response["Data"] as! String == "Like"{
                                        sender.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
                                    }
                                    else
                                    {
                                        sender.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                                    }
                                    
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                                }
                            }
                        }
                    }
                }, failure: { (error) in
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                    }
                })
            }
            
        }
        else{
            
        }
      
    }
    @IBAction func moreButtonClicked(sender: UIButton)  {
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        if Int(UserDefaults.standard.value(forKey: "userid") as! String) == dictDetail["user_id"] as? Int {
            if dictDetail["post_type"] as! Int == 1 {
                // The list of items to display. Can be changed dynamically
                dropDown.dataSource = ["Delete Post", "Edit Post"]
            }
            else{
                // The list of items to display. Can be changed dynamically
                dropDown.dataSource = ["Delete Post"]
            }
        }
        else{
            dropDown.dataSource = ["Report Abuse"]
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if index == 0 {
                if item == "Report Abuse" {
                    if self.dictDetail["report_abuse_status"] as! Int == 0 {
                        self.reportAbuseAPICalled(dictPost: self.dictDetail)
                    }
                    else{
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Already report this post", on: self)
                    }
                }
                else{
                    self.deletePostAlert(dictPost: self.dictDetail)
                }
            }
            else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
                vc.dictPost = self.dictDetail
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        dropDown.show()
    }
    func reportAbuseAPICalled(dictPost:NSDictionary) {
        self.reportToAbuseViuew.isHidden = false
    }
    func deletePostAlert(dictPost:NSDictionary) {
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: GlobalConstant.AppName, message: "Are you sure you want to delete this post?", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Yes", style: .cancel) { action -> Void in
            //Do some stuff
            self.deletePostApiCalled(dict: dictPost)
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "No", style: .default) { action -> Void in
            //Do some other stuff
        }
        actionSheetController.addAction(nextAction)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func deletePostApiCalled(dict :NSDictionary) {
        let dictParam : NSDictionary = ["post_id":String.init(format: "%d", dict["post_id"] as! Int)]
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)delete_post", strType: "delete_post", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    @IBAction func likeButtonClicked(sender: UIButton)  {
        
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
           
            let count = Int(sender.title(for: .normal)!)
            if (sender.currentImage == #imageLiteral(resourceName: "like_Blue")){
                sender.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
                if count != 0 {
                    sender.setTitle("\(count!-1)", for: .normal)
                }
            }
            else{
                sender.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
                sender.setTitle("\(count!+1)", for: .normal)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", self.dictDetail["post_id"] as! Int)]
                appdelegate.vibrateMobile()
                WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)like_post", strType: "like_post", dictParameter: dictParam, controllerView: self, success: { (response) in
                    print(response)
                    if response.count == 0 {
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                        }
                    }
                    else{
                        if response["Status"] is Int{
                            if response["Status"] as! Int == 200 {
                                DispatchQueue.main.async {
                                    if response["Data"] as! String == "Like"{
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                                }
                            }
                        }
                    }
                }, failure: { (error) in
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                    }
                })
            }
            
        }else{
            
            
        }
        
        
    }
    @IBAction func shareButtonClicked(sender: UIButton)  {
        let text = dictDetail["title"] as! String
        let myWebsite = NSURL(string:dictDetail["post_image"] as! String)
        let shareAll = [text ,myWebsite ?? ""] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func didTappedonSendComment(_ sender: AnyObject) {
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "E MMM d HH:mm:ss Z yyyy"
            let currentDate = formatter.string(from: date)
            
            if txtComment.text != "" || imageData != nil {
                var dictParam : NSDictionary = [:]
                
                var encoded = txtComment.text?.toBase64()
                let remainder = (encoded?.count)! % 4
                if remainder > 0 {
                    encoded = encoded?.padding(toLength: (encoded?.count)! + 4 - remainder,
                                               withPad: "=",
                                               startingAt: 0)
                    print(encoded!)
                }
                if strCommentType == "1" {
                    if (imageData != nil) {
                        
                        dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"comment_type":"1","message":encoded!,"image":imageData, "add_date":currentDate]
                        
                    } else {
                        
                        dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"comment_type":"1","message":encoded!,"add_date":currentDate]
                    }
                } else if nested_comment_id != "" {
                    
                    if (imageData != nil) {
                        
                        dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"comment_type":"2","message":encoded!,"comment_id":strCommentID,"nested_comment_id":nested_comment_id,"image":imageData, "add_date":currentDate]
                        
                    } else {
                        
                        dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"comment_type":"2","message":encoded!,"comment_id":strCommentID,"nested_comment_id":nested_comment_id, "add_date":currentDate]
                        
                    }
                } else if strCommentType == "2" {
                    
                    if (imageData != nil) {
                        
                        dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"comment_type":"2","message":encoded!,"comment_id":strCommentID,"image":imageData, "add_date":currentDate]
                        
                    } else {
                        
                        dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"comment_type":"2","message":encoded!,"comment_id":strCommentID, "add_date":currentDate]
                    }
                }
                print("My Params:", dictParam)
                
                WebHelper.requestUrlForPostWithType("\(GlobalConstant.CommentURL)add_comment", strType: "add_comment", dictParameter: dictParam, controllerView: self, success: { (response) in
                    print(response)
                    if response.count == 0 {
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                        }
                    } else {
                        
                        if response["Status"] is Int {
                            
                            if response["Status"] as! Int == 200 {
                                
                                DispatchQueue.main.async {
                                    self.view.endEditing(true)
                                    self.txtComment.text = ""
                                    self.isthereMoreData = true
                                    self.getAllComment()
                                    self.imageData = nil
                                }
                            } else {
                                DispatchQueue.main.async {
                                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                                }
                            }
                        }
                    }
                }, failure: { (error) in
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                    }
                })
            }
            else{
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please say something!", on: self)
            }
            
        }else{
            
        }
                
       
    }
        
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.reportToAbuseViuew.isHidden = true
    }
    
    @IBAction func okButttonClicked(_ sender: Any) {
        if abuseTextview.text == "" || abuseTextview.text == "Write here..." {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter report", on: self)
            return
        }
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", self.dictDetail["post_id"] as! Int),"message":abuseTextview.text]
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)report_abous", strType: "report_abous", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            self.reportToAbuseViuew.isHidden = true
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    //MARK: - Comment Image Picker
    @IBAction func didTappedonUploadImage(_ sender: Any) {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: NSLocalizedString("Pick Image", comment: ""), message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { _ in
            print("Annuleer")
            self.dismiss(animated: true, completion: nil)
            
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title:  NSLocalizedString("Camera", comment: ""), style: .default)
        { _ in
            print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera;
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            else{
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Camera not available", on: self)
            }
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default)
        { _ in
            print("Galerij")
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum;
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            else{
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Gallery not available", on: self)
            }
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(image, 0.1)
            self.didTappedonSendComment(self)
        } else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension PostDetailViewController : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrAnswer.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        cell.lblName.text = arrOption[indexPath.row]
        let strAnsID = (arrAnswer[indexPath.row] as! NSDictionary)["id"] as! String
        cell.lbladdress.text = strAnsID
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width/2, height: 30)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
extension PostDetailViewController : UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrComment.count == 0 ? 1 : arrComment.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! TableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if arrComment.count == 0 {
            
            let header = tableView.dequeueReusableCell(withIdentifier: "NoDataCell") as! TableViewCell
            return header
        }
        
        let header = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! TableViewCell
        
        let dictComment = arrComment[section] as! NSDictionary
        print(dictComment)
        header.lblName.text = dictComment["user_name"] as? String
        
        let commentDate = dictComment["date"] as! String
        if commentDate != "" {
            
            let date = GlobalConstant.getDateFromStringWithFormate(strFormate: "E MMM d HH:mm:ss Z yyyy", strDate: dictComment["date"] as! String)
            header.lblDateTime.text = Date().offset(from: date)
        }
        

       
        
        if dictComment["message"] as! String != "" {
            header.viewComment.isHidden = false
            header.imgTop.constant = 3
            let decodeText = dictComment["message"] as? String
            let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
            if let decoded = msg.fromBase64() {
                header.lblComment.text = decoded
            }else{
                header.lblComment.text = dictComment["message"] as? String
            }// "A test string."
        }
        else{
            header.viewComment.isHidden = true
            header.imgTop.constant = -35
        }
        
        
        header.imgIcon.isHidden = true
        if dictComment["image"] as! String != "" {
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewClicked(_:)))
            header.imgIcon.isUserInteractionEnabled = true
            header.imgIcon.addGestureRecognizer(tapGestureRecognizer)
            header.imgIcon.isHidden = false
            
            header.imgIcon.af_setImage(withURL: URL.init(string: dictComment["image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
        }
        
        
        if (dictComment["nested_comment"] as! NSArray).count > 0 {
            header.lblView.isHidden = false
        }else{
            header.lblView.isHidden = true
        }
        
        
        if dictComment["like_status"] as! Int == 1 {
            header.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
        }else{
            header.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
        }
        
        header.btnLike.setTitle(String.init(format: "%d", dictComment["count_like"] as! Int), for: .normal)
        header.btnLike.tag = section
        header.btnLike.addTarget(self, action: #selector(mainCommentlikeButtonClicked(sender:)), for: .touchUpInside)
        header.btnComment.tag = section
        header.btnComment.addTarget(self, action: #selector(seeAllButtonClicked(_:)), for: .touchUpInside)
        header.lblView.tag = section
        header.lblView.addTarget(self, action: #selector(seeAllButtonClicked(_:)), for: .touchUpInside)
        header.btnComment.setTitle(String.init(format: "%d", dictComment["count_comment"] as! Int), for: .normal)
        header.lblDescription.isHidden = true
        
        if (dictComment["user_id"] as? Int == postOwnerID) {
            
            header.lblDescription.isHidden = false
        }
        
        if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
            header.btnLike.isEnabled = false
            header.btnComment.isEnabled = false
            
            header.btnLike.alpha = 0.5
            header.btnComment.alpha = 0.5
        }
        
        return header
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let searchlbl:UILabel = (sender.view as! UILabel)
        if let urll = URL(string: "\(searchlbl.text!)") {
            UIApplication.shared.open(urll, options: [:])
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        if arrComment.count == 0 {
            return 40
        }else{
            let dictComment = arrComment[section] as! NSDictionary
            if(dictComment["message"] as! String == ""){
                let hgt = (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90 + 50
                return hgt
                
            }
            
            if(dictComment["image"] as! String == ""){
                let hgt = (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90
                return hgt
                
            }
            
            if((dictComment["image"] as! String != "") && (dictComment["message"] as! String != "")){
                
                let hgt = (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90 + 50
                return hgt
            }
            else{
                return (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90
            }
        }
        
    }

    @objc func seeAllButtonClicked(_ sender: UIButton) {
        
        let dictComment = arrComment[sender.tag] as! NSDictionary
        let comment = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        comment.dictPollDetail = dictDetail
        comment.comment_id = String.init(format: "%d", dictComment["comment_id"] as! Int)
        self.navigationController?.pushViewController(comment, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 800
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if self.isthereMoreData == true {
            self.getAllComment()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrComment.count-1 {
            if self.isthereMoreData == true {
                self.getAllComment()
            }
        }
    }
    
    @objc func mainCommentlikeButtonClicked(sender: UIButton) {
        
        let count = Int(sender.title(for: .normal)!)
        if (sender.currentImage == #imageLiteral(resourceName: "like_Blue")){
            sender.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if count != 0 {
                sender.setTitle("\(count!-1)", for: .normal)
            }
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            sender.setTitle("\(count!+1)", for: .normal)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            let dictComment = self.arrComment[sender.tag] as! NSDictionary
            let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", self.dictDetail["post_id"] as! Int),"comment_id":dictComment["comment_id"] as! Int,"like_cooment_type":"1"]
        
        appdelegate.vibrateMobile()
        WebHelper.requestForReset_OTPPostUrl("\(GlobalConstant.CommentURL)like_comment", dictParameter: dictParam, controllerView: self, success: { (response) in
            
            print(response)
            
            if response.count == 0 {
                
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            
                           
                            if response["Data"] as! String == "Like"{
                                
                            }
                            else
                            {
                                
                            }
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
        }
    }
    
    @IBAction func clickedOnComment(_ sender: Any){
        txtComment.becomeFirstResponder()
    }
    
}

extension PostDetailViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        strCommentType = "1"
        nested_comment_id = ""
    }
}
