//
//  CommentViewController.swift
//  H1BQ
//
//  Created by mac on 03/08/18.
//  Copyright © 2018 mac. All rights reserved.
//

import UIKit

class CommentViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    var imagePicker = UIImagePickerController()
    var dictPollDetail:NSDictionary!
    var comment_id : String!
    @IBOutlet var tblComment: UITableView!
    var dictMainComment : NSDictionary = [:]
    var arrNestedCommnt : [NSDictionary] = []
    
    @IBOutlet var txtComment: UITextField!
    var strCommentType:String! = "2"
    var nested_comment_id :String! = ""
    var postOwnerID : Int!
    var imageData:Data?
    @IBOutlet var zoomView: UIView!
    @IBOutlet var imgViewZoom: UIImageView!
    
    @IBOutlet weak var imgTop : NSLayoutConstraint!
    
    
    
    @IBOutlet weak var viewHeader : UIView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblDateTime : UILabel!
    @IBOutlet weak var btnLike : UIButton!
    @IBOutlet weak var btnComment : UIButton!
    @IBOutlet weak var lblComment : UITextView!
    @IBOutlet weak var viewComment : UIView!
    @IBOutlet weak var imgIcon : UIImageView!
    
    @IBOutlet weak var viewHeadHeight : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getAllComment()
        self.tblComment.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if timerTest != nil {
            timerTest?.invalidate()
            timerTest = nil
        }
    }
    
    func getAllComment() {
        postOwnerID = dictPollDetail["user_id"] as! Int
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictPollDetail["post_id"] as! Int),"comment_id":comment_id]
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.CommentURL)load_nested_comment", strType: "load_nested_comment", dictParameter: dictParam,  controllerView: self, success: { (response) in
            
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            
                            let resp = (response["Data"] as! [NSDictionary]).first
                            self.arrNestedCommnt = resp!["nested_comment"] as! [NSDictionary]
                            self.dictMainComment = (response["Data"] as! [NSDictionary]).first!
                            self.setCommentOnview()
                            self.tblComment.reloadData()
                        }
                    } else {
                        DispatchQueue.main.async {
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    @IBAction func Back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func didTappedonSendComment(_ sender: AnyObject) {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "E MMM d HH:mm:ss Z yyyy"
        let currentDate = formatter.string(from: date)
        
        if txtComment.text != "" || imageData != nil{
            var dictParam : NSDictionary = [:]
            
            //base 64
            var encoded = txtComment.text?.toBase64()
            let remainder = (encoded?.count)! % 4
            if remainder > 0 {
                encoded = encoded?.padding(toLength: (encoded?.count)! + 4 - remainder,
                                           withPad: "=",
                                           startingAt: 0)
                print(encoded!)
            }
            
            if strCommentType == "1" {
                
                dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictPollDetail["post_id"] as! Int),"comment_type":"1","message":encoded!,"image":imageData, "add_date":currentDate]
                
            } else if nested_comment_id != "" {
                
                dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictPollDetail["post_id"] as! Int),"comment_type":"2","message":encoded!,"comment_id":comment_id,"nested_comment_id":nested_comment_id,"image":imageData, "add_date":currentDate]
                
            } else if strCommentType == "2" {
                
                dictParam = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictPollDetail["post_id"] as! Int),"comment_type":"2","message":encoded!,"comment_id":comment_id,"image":imageData, "add_date":currentDate]
            }
            
            print("My Params:", dictParam)
            
            WebHelper.requestUrlForPostWithType("\(GlobalConstant.CommentURL)add_comment", strType: "add_comment", dictParameter: dictParam, controllerView: self, success: { (response) in
                print(response)
                if response.count == 0 {
                    DispatchQueue.main.async {
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                    }
                } else {
                    
                    if response["Status"] is Int {
                        
                        if response["Status"] as! Int == 200 {
                            
                            DispatchQueue.main.async {
                                
                                self.view.endEditing(true)
                                self.txtComment.text = ""
                                self.getAllComment()
                                self.imageData = nil
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                            }
                        }
                    }
                }
            }, failure: { (error) in
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                }
            })
        }
        else {
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please say something!", on: self)
        }
    }
    
    @IBAction func imageViewClicked(_ sender: UIGestureRecognizer) {
        zoomView.isHidden = false
        if (sender.view as? UIImageView)?.tag == 200 {
            imgViewZoom.image = (sender.view as? UIImageView)?.image
        }
    }
    
    @IBAction func hideButtonclickedonZoomView(_ sender: UIButton) {
        
        zoomView.isHidden = true
    }
    
    //MARK: - Comment Image Picker
    
    @IBAction func didTappedonUploadImage(_ sender: Any) {
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: NSLocalizedString("Pick Image", comment: ""), message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { _ in
            print("Annuleer")
            self.dismiss(animated: true, completion: nil)
            
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title:  NSLocalizedString("Camera", comment: ""), style: .default)
        { _ in
            print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera;
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
                
            } else {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Camera not available", on: self)
            }
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default)
        { _ in
            
            print("Gallery")
            
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = true
                
                self.present(self.imagePicker, animated: true, completion: nil)
                
            } else {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Gallery not available", on: self)
            }
        }
        actionSheetControllerIOS8.addAction(deleteActionButton)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageData = UIImageJPEGRepresentation(image, 0.1)
            self.didTappedonSendComment(self)
            
        } else {
            
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CommentViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func setCommentOnview(){
        
        lblName.text = dictMainComment["user_name"] as? String
        
        if dictMainComment["message"] as! String != ""  && dictMainComment["image"] as! String == "" {
            self.viewComment.isHidden = false
            imgIcon.isHidden = true
            let decodeText = dictMainComment["message"] as? String
            let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
            if let decoded = msg.fromBase64() {
                lblComment.text = decoded
            }else{
                lblComment.text = dictMainComment["message"] as? String
            }
            self.viewHeadHeight.constant = (lblComment.contentSize.height) + 130
        }
        
         if dictMainComment["message"] as! String == "" && dictMainComment["image"] as! String != ""{
            self.viewComment.isHidden = true
            self.imgTop.constant = -45
            imgIcon.isHidden = false
            imgIcon.af_setImage(withURL: URL.init(string: dictMainComment["image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
            self.viewHeadHeight.constant = 140
        }
        
        if dictMainComment["message"] as! String != "" && dictMainComment["image"] as! String != ""{
            self.viewComment.isHidden = false
            self.imgTop.constant = 3
            imgIcon.isHidden = false
            
            imgIcon.af_setImage(withURL: URL.init(string: dictMainComment["image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
            
            let decodeText = dictMainComment["message"] as? String
            let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
            if let decoded = msg.fromBase64() {
                lblComment.text = decoded
            }else{
                lblComment.text = dictMainComment["message"] as? String
            }
            
            self.viewHeadHeight.constant = lblComment.contentSize.height + 150
        }
        
        
        
        let date = GlobalConstant.getDateFromStringWithFormate(strFormate: "E MMM d HH:mm:ss Z yyyy", strDate: dictMainComment["date"] as! String)
        lblDateTime.text = Date().offset(from: date)
        
        if dictMainComment["like_status"] as! Int == 1 {
            btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            
        } else {
            btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
        }
        
        btnLike.setTitle(String.init(format: "%d", dictMainComment["count_like"] as! Int), for: .normal)
        
        btnComment.setTitle(String.init(format: "%d", dictMainComment["count_comment"] as! Int), for: .normal)
        lblDescription.isHidden = true
        if (dictMainComment["user_id"] as? Int == postOwnerID) {
            lblDescription.isHidden = false
        }
    }
    
    
    @IBAction func mainCommentlikeButtonClicked(sender: UIButton)  {
        
        let count = Int(sender.title(for: .normal)!)
        if (sender.currentImage == #imageLiteral(resourceName: "like_Blue")){
            sender.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if count != 0 {
                sender.setTitle("\(count!-1)", for: .normal)
            }
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            sender.setTitle("\(count!+1)", for: .normal)
        }
        
        print(dictMainComment)
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictPollDetail["post_id"] as! Int),"comment_id":dictMainComment["comment_id"] as! Int,"like_cooment_type":"1"]
        appdelegate.vibrateMobile()
        WebHelper.requestForReset_OTPPostUrl("\(GlobalConstant.CommentURL)like_comment", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            if response["Data"] as! String == "Like"{
                                //self.viewDidLoad()
                            }
                            else
                            {
                                   // self.viewDidLoad()
                               // }
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNestedCommnt.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! TableViewCell
        
        let dictComment = self.arrNestedCommnt[indexPath.row]
        print(dictComment)
        
        
        if dictComment["message"] as! String != "" {
            cell.viewComment.isHidden = false
            cell.imgTop.constant = 3
            cell.lblName.text = dictComment["user_name"] as? String
            let decodeText = dictComment["message"] as? String
            let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
            if let decoded = msg.fromBase64() {
                cell.lblComment.text = decoded
            }else{
                cell.lblComment.text = dictComment["message"] as? String
            }// "A test st string."
        }
        else{
            cell.viewComment.isHidden = true
            cell.imgTop.constant = -35
        }
        
        let date = GlobalConstant.getDateFromStringWithFormate(strFormate: "E MMM d HH:mm:ss Z yyyy", strDate: dictComment["date"] as! String)
        cell.lblDateTime.text = Date().offset(from: date)
        
        cell.imgIcon.isHidden = true
        if dictComment["image"] as! String != "" {
            cell.imgIcon.isHidden = false
            cell.imgIcon.af_setImage(withURL: URL.init(string: dictComment["image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
            cell.imgIcon.isUserInteractionEnabled = true
            cell.imgIcon.addGestureRecognizer(tapGestureRecognizer)
        }
        if dictComment["like_status"] as! Int == 1 {
            cell.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
        }else{
            cell.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
        }
        cell.btnLike.setTitle(String.init(format: "%d", dictComment["count_like"] as! Int), for: .normal)
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(nestedCommentlikeButtonClicked(sender:)), for: .touchUpInside)
        cell.btnComment.setTitle(String.init(format: "%d", dictComment["count_comment"] as! Int), for: .normal)
        cell.btnReply.addTarget(self, action: #selector(cellReplyButtonClicked(sender:)), for: .touchUpInside)
        cell.lblDescription.isHidden = true
        if (dictComment["user_id"] as? Int == postOwnerID) {
            cell.lblDescription.isHidden = false
        }
        return cell
    }
    
    
    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer){
        zoomView.isHidden = false
        if (tapGestureRecognizer.view as? UIImageView)?.tag == 200 {
            imgViewZoom.image = (tapGestureRecognizer.view as? UIImageView)?.image
        }
    }
   
    
    
    @objc func cellReplyButtonClicked(sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblComment)
        let indexPath = tblComment.indexPathForRow(at:buttonPosition)!
        let dictComment = self.arrNestedCommnt[indexPath.row]
        strCommentType = "2"
        nested_comment_id = String.init(format: "%d", dictComment["nested_comment_id"] as! Int)
        txtComment.becomeFirstResponder()
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let dictComment = self.arrNestedCommnt[indexPath.row]
        if(dictComment["message"] as! String == ""){
            let hgt = (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90 + 50
            return hgt
            
        }
        
        if(dictComment["image"] as! String == ""){
            let hgt = (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90
            return hgt
            
        }
        
        if((dictComment["image"] as! String != "") && (dictComment["message"] as! String != "")){
            
            let hgt = (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90 + 50
            return hgt
        }
        else{
            return (dictComment["message"] as! String).height(withConstrainedWidth: tableView.frame.size.width-100, font: UIFont.init(name: "OpenSans-Regular", size: 12)!)+90
        }
    }
    
    
  
    @IBAction func headerReplyButtonClicked(sender : UIButton) {
        strCommentType = "2"
        nested_comment_id = ""
        txtComment.becomeFirstResponder()
    }
    
    @objc func nestedCommentlikeButtonClicked(sender: UIButton)  {
        
        let count = Int(sender.title(for: .normal)!)
        if (sender.currentImage == #imageLiteral(resourceName: "like_Blue")){
            sender.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if count != 0 {
                sender.setTitle("\(count!-1)", for: .normal)
            }
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            sender.setTitle("\(count!+1)", for: .normal)
        }
        
        let dictComment = self.arrNestedCommnt[sender.tag]
        print(dictComment)
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictPollDetail["post_id"] as! Int),"comment_id":dictComment["nested_comment_id"] as! Int,"like_cooment_type":"2"]
        
        appdelegate.vibrateMobile()
        WebHelper.requestForReset_OTPPostUrl("\(GlobalConstant.CommentURL)like_comment", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            if response.count == 0 {
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            
                            if response["Data"] as! String == "Like"{
                            }
                            else
                            {
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }

}
extension CommentViewController : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        strCommentType = "2"
        nested_comment_id = ""
    }
}
