import UIKit
import AlamofireImage
import DropDown
import AVFoundation
import AVKit
import GoogleMobileAds
import RealmSwift
import SVProgressHUD
import SystemConfiguration
import Branch


var timerTest : Timer?
var tblRowcount = 0

class HomeVC: UIViewController , UIGestureRecognizerDelegate , BranchShareLinkDelegate{
    @IBOutlet var headercollectionView: UICollectionView!
    @IBOutlet var tblViewList: UITableView!
    @IBOutlet var reportToAbuseViuew: UIView!
    @IBOutlet var abuseTextview: UITextView!
    @IBOutlet var lblNoFeed: UILabel!
    
    var arrAddImages : [NSDictionary] = []
    var currentItem:PostListRealm?
    let realm = try? Realm()
    var isInternetConnection = ""
    
    
    
    var audioPlayer = AVAudioPlayer()
    var arrHeader = ["H1B","PERM","I-140","I-131(AP)","I-485","I-765(EAD)","MISC"]
    
    @IBOutlet weak var btnScrollTop: UIButton!
    var selectIndex = 0
    var scrollIndex = 0
    var strBucketID : String = "1"
    var arrPostList : NSMutableArray = []
    var isthereMoreData: Bool = false
    var arrOption : [String] = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var refreshControl:UIRefreshControl!
    
    var dictDetail: NSDictionary!
    @IBOutlet weak var adMobView: GADBannerView!
    
    @IBOutlet weak var AddView : UIView!
    @IBOutlet weak var txtAddView : UITextView!
    @IBOutlet weak var txtHeightCons : NSLayoutConstraint!
    @IBOutlet weak var imgAdd : UIImageView!
    
    var arrDataBase : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isBack = ""
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        AddView.isHidden = true
        
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
            self.callAPIForCheckActiveORnot()
        }
        
        btnScrollTop.isHidden = true
        let music = Bundle.main.path(forResource: "newMessage", ofType: "wav")
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: music! ))
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch{
            print(error)
        }
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblViewList!.addSubview(refreshControl)
        
        adMobView.delegate = self
        adMobView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adMobView.rootViewController = self
        adMobView.load(GADRequest())
        
    }
    
    @objc func refresh(sender:AnyObject) {
        appdelegate.vibrateMobile()
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
            self.getPostByBucketID(pooltoRefresh: true)
        }else{
            
            let alert = UIAlertController(title: GlobalConstant.AppName, message: "No internet connection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                    self.tblViewList.reloadData()
                    self.refreshControl.endRefreshing()
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
        self.present(alert, animated: true, completion: nil)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadDataFromApiFirstTime(){
        
        SVProgressHUD.show()
        let arr = ["1","2","3","4","5","6","7",]
        for i in 0..<arr.count{
            self.callgetPostByAllId(bucketid: "\(i+1)")
            
        }
        SVProgressHUD.dismiss()
        userDef.set("session", forKey: "session")
    }
    
    
    func callgetPostByAllId(bucketid : String){
      
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"offset":"0","buket_id": bucketid]
        WebHelper.requestForReset_OTPPostUrl("\(GlobalConstant.PostURL)post_list", dictParameter: dictParam, controllerView: self, success: { (response) in
            
            print(response)
            if response.count == 0 {
                
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            
                            let arr = response["Data"] as! NSArray
                            var intCount = 0
                            
                            for dict in arr {
                                
                                self.arrPostList.add(dict as! NSDictionary)
                                self.saveListinRealm(dict: dict as! NSDictionary)
                                
                                
                                intCount += 1
                                
                                if intCount == 10 {
                                    
                                    intCount = 0
                                    self.arrPostList.add(["post_type":3])
                                }
                            }
                            
                            if arr.count == 0 {
                                self.isthereMoreData = false
                            }
                            self.tblViewList.isHidden = false
                            self.headercollectionView.isHidden = false
                            self.tblViewList.reloadData()
                            self.headercollectionView.reloadData()
                            self.refreshControl.endRefreshing()
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        if(isBack == "true"){
//            self.tblViewList.isHidden = false
//            //self.lblNoFeed.isHidden = true
//
//        }
//        else{
        
            self.arrPostList = []
            if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
                
                print("networkkkkk")
                isInternetConnection = "true"
                if(!isCallDataOnce){
                    self.loadDataFromApiFirstTime()
                    
                }else{
                    if(appdelegate.checkBucket == "present"){
                        self.tblViewList.isHidden = true
                        self.lblNoFeed.isHidden = true
                        self.headercollectionView.isHidden = true
                        selectIndex = appdelegate.selectedCat
                        print("Home Screen = \(appdelegate.selectedCat)")
                        strBucketID = "\(selectIndex+1)"
                        self.getPostByBucketID(pooltoRefresh: true)
                    }
                    else{
                        self.tblViewList.isHidden = true
                        self.lblNoFeed.isHidden = true
                        selectIndex = appdelegate.selectedCat
                        print("Home Screen = \(appdelegate.selectedCat)")
                        strBucketID = "\(selectIndex+1)"
                        self.getPostByBucketID(pooltoRefresh: true)
                    }
                    
                    timerTest = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(HomeVC.callAdvertismentApi), userInfo: nil, repeats: true)
                }
                
            }
            else{
                
                isInternetConnection = "false"
                
                if timerTest != nil {
                    timerTest?.invalidate()
                    timerTest = nil
                }
                
                print("no networkkkk")
                let arrDBList = DBManager.sharedInstance.getDataFromDB()
                if(arrDBList.count > 0){
                    print(arrDBList)
                    for i in 0..<arrDBList.count{
                        
                        let dict = arrDBList[i]
                        self.arrPostList.add(dict)
                    }
                    print(self.arrPostList)
                }
                else{
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "No data.Please check Internet", on: self)
                }
                
            }
        //}
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
       
    }

    func getPostByBucketID(pooltoRefresh:Bool)  {
        if pooltoRefresh == true {
            scrollIndex = 0
        }
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"offset":"\(scrollIndex)","buket_id": strBucketID]
        WebHelper.requestForReset_OTPPostUrl("\(GlobalConstant.PostURL)post_list", dictParameter: dictParam, controllerView: self, success: { (response) in
            
            print(response)
            if response.count == 0 {
                
                DispatchQueue.main.async {
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                       // DispatchQueue.main.async {
                        
                        OperationQueue.main.addOperation {
                            if pooltoRefresh == true {
                                self.arrPostList = []
                                self.isthereMoreData = true
                            }
                            
                            let arr = response["Data"] as! NSArray
                            var intCount = 0
                            
                            for dict in arr {
                                
                                self.arrPostList.add(dict as! NSDictionary)
                                self.saveListinRealm(dict: dict as! NSDictionary)
                                
                                
                                intCount += 1
                                
                                if intCount == 10 {
                                    
                                    intCount = 0
                                    self.arrPostList.add(["post_type":3])
                                }
                            }
                            
                            if arr.count == 0 {
                                
                                self.isthereMoreData = false
                            }
                            self.scrollIndex += 1
                            self.tblViewList.isHidden = false
                            self.headercollectionView.isHidden = false
                            self.tblViewList.reloadData()
                            self.headercollectionView.reloadData()
                          //  self.tblViewList.setContentOffset(.zero, animated: true)
                            
                            self.refreshControl.endRefreshing()
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    
    @IBAction func btnTopToMove(_ sender: Any) {
        scrollToTop()
    }
    
    func scrollToTop() -> Void {
        DispatchQueue.main.async {
            let indexPath = NSIndexPath(row: 0, section: 0)
            self.tblViewList.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.reportToAbuseViuew.isHidden = true
    }
    
    @IBAction func okButttonClicked(_ sender: Any) {
        
        if abuseTextview.text == "" || abuseTextview.text == "Write here..." {
            
            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Please enter report", on: self)
            return
        }
        
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dictDetail["post_id"] as! Int),"message":abuseTextview.text]
        
        
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)report_abous", strType: "report_abous", dictParameter: dictParam, controllerView: self, success: { (response) in
            
            print(response)
            
            if response.count == 0 {
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                if response["Status"] is Int {
                    
                    if response["Status"] as! Int == 200 {
                        
                        DispatchQueue.main.async {
                            
                            self.reportToAbuseViuew.isHidden = true
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    } else {
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    
    func saveListinRealm(dict : NSDictionary){
        let item = PostListRealm(value: dict)
        if(currentItem == nil) {
            item.post_id = dict["post_id"] as! Int
        }
        DBManager.sharedInstance.addData(object: item)
        
    }
    
    

    
}

////Advertisement Api
extension HomeVC {
    
    @objc func callAdvertismentApi(){
        WebHelper.requestGetUrl(GlobalConstant.Advertisment_URL, controllerView: self, success: { (response) in
            self.arrAddImages = response["Data"] as! [NSDictionary]
            if(self.arrAddImages.count > 0){
                self.CallRandomArrayVal()
            }
        }) { (error) in
            
            print(error.debugDescription)
        }
        
    }
    
    func CallRandomArrayVal(){
        OperationQueue.main.addOperation {
            self.AddView.isHidden = false
            let randomIndex = Int(arc4random_uniform(UInt32(self.arrAddImages.count)))
            let dict = self.arrAddImages[randomIndex]
            let imgUrl = "\(dict["base_url"] as! String)\(dict["advertisement_image"] as! String)"
            self.imgAdd.af_setImage(withURL: URL.init(string: imgUrl)!, placeholderImage:UIImage.init(named: "image.jpg"))
            
            
            
            if(dict["description"] as? String == ""){
                self.txtAddView.text = "no description"
                self.adjustUITextViewHeight(arg: self.txtAddView)
            }else{
                self.txtAddView.text = dict["description"] as? String
                self.adjustUITextViewHeight(arg: self.txtAddView)
            }
            
            
        }
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    @IBAction func clickOnCrossAddViewBtn(_ sender : UIButton){
        AddView.isHidden = true
    }
}

extension HomeVC : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrHeader.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        cell.lblName.text = arrHeader[indexPath.row]
        cell.lblName.textColor = UIColor.white
        cell.lblName.backgroundColor = UIColor.clear
        if selectIndex == indexPath.row {
            cell.lblName.textColor = GlobalConstant.hexStringToUIColor("0294D6")
            cell.lblName.backgroundColor = UIColor.white
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 70, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.tblViewList.isHidden = true;
        selectIndex = indexPath.row
        collectionView.reloadData()
        strBucketID = "\(selectIndex+1)"
        
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                self.tblViewList.isHidden = false
              //  self.arrPostList = []
                self.getPostByBucketID(pooltoRefresh: true)
            }
            
        }
        else{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
           self.tblViewList.isHidden = false
            self.tblViewList.reloadData()
            }
        }
        
        
        
        // get current level:
        let  audioSession = AVAudioSession.sharedInstance()
        let volume : Float = audioSession.outputVolume
        print(String(format: "output volume: %1.2f dB", 20.0 * log10f(volume + .leastNormalMagnitude)))

        self.playSound()
        
    }
    
    func playSound()  {
        audioPlayer.play()
    }
}
extension HomeVC : UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isInternetConnection == "true"){
        return self.arrPostList.count
        }
        else{
            
            let allItems = DBManager.sharedInstance.getDataFromDB()
            let kkk = allItems.filter("bucket == \(strBucketID)", self)
            print(kkk)
            
           // let item = realm?.objects(PostListRealm.self).filter("bucket == \(strBucketID)")[section] as! PostListRealm
           // arrDataBase.add(kkk)
            return kkk.count
        }
    }
    
    
    func isValidString(testStr:String) -> Bool {
        let emailRegEx = "([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if(self.arrPostList.count>0){
             if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{
               let dict  = self.arrPostList[indexPath.row] as! NSDictionary
                print(dict)
                if dict["post_type"] as! Int == 1 {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! TableViewCell
                    
                    
                    
                    if dict["bookmark_status"] as! Int == 0 {
                        cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                    } else {
                        cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
                    }
                    cell.btnBookmark.tag = indexPath.row
                    cell.btnBookmark.addTarget(self, action: #selector(bookmarkButtonClicked(sender:)), for: .touchUpInside)
                    cell.btnMore.tag = indexPath.row
                    cell.btnMore.addTarget(self, action: #selector(moreButtonClicked(sender:)), for: .touchUpInside)
                    cell.lblName.text = dict["username"] as? String ?? ""
                    cell.lblTitle.text = dict["title"] as? String ?? ""
                    
                    
                    let decodeText = dict["dicription"] as? String
                    let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
                    if let decoded = msg.fromBase64() {
                        cell.lblDescription.text = decoded
                    }else{
                        cell.lblDescription.text = dict["dicription"] as? String
                    }
                    
                    if(cell.lblDescription.text.count > 55){
                        cell.lblDescription.textContainer.maximumNumberOfLines = 2
                        cell.lblDescription.textContainer.lineBreakMode = .byTruncatingTail
                        cell.lblSeeMore.text = "See More"
                    }else{
                        cell.lblDescription.textContainer.lineBreakMode = .byWordWrapping
                        cell.lblSeeMore.text = ""
                    }
                    
                    
                    cell.imgBG.image = nil
                    cell.imgBG.af_cancelImageRequest()
                    if dict["post_image"] as! String != "" {
                        
                        cell.imgBG.af_setImage(withURL: URL.init(string: dict["post_image"] as! String)!, placeholderImage:UIImage.init(named: "image.jpg"))
                    }
                    let date = GlobalConstant.getDateFromString(strDate: dict["add_date"] as! String)
                    cell.lblDateTime.text = Date().offset(from: date)
                    cell.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
                    if dict["like_status"] as! Int == 1 {
                        cell.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
                    }
                    cell.btnLike.setTitle(String.init(format: "%d", dict["count_like"] as! Int), for: .normal)
                    cell.btnLike.tag = indexPath.row
                    cell.btnLike.addTarget(self, action: #selector(likeButtonClicked(sender:)), for: .touchUpInside)
                    cell.btnComment.tag = indexPath.row
                    cell.btnComment.setTitle(String.init(format: "%d", dict["count_comment"] as! Int), for: .normal)
                    cell.btnComment.addTarget(self, action: #selector(commentButtonClicked(sender:)), for: .touchUpInside)
                    cell.btnViewAll.setTitle(String.init(format: "%d", dict["count_view"] as! Int), for: .normal)
                    cell.btnContact.tag = indexPath.row
                    cell.btnContact.addTarget(self, action: #selector(contactButtonClicked(sender:)), for: .touchUpInside)
                    
                    
                    cell.btnShare.tag = indexPath.row
                    cell.btnShare.addTarget(self, action: #selector(shareButtonClicked(sender:)), for: .touchUpInside)
                    
                    
                    if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
                        cell.btnLike.isEnabled = false
                        cell.btnComment.isEnabled = false
                        cell.btnShare.isEnabled = false
                        cell.btnBookmark.isEnabled = false
                        cell.btnMore.isEnabled = false
                        
                        cell.btnLike.alpha = 0.5
                        cell.btnComment.alpha = 0.5
                        cell.btnShare.alpha = 0.5
                        cell.btnBookmark.alpha = 0.5
                        cell.btnMore.alpha = 0.5
                    }
                    
                    
                    
                    return cell
                    
                } else if dict["post_type"] as! Int == 2 {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PollCell", for: indexPath) as! TableViewCell
                    if dict["bookmark_status"] as! Int == 0 {
                        cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                    } else {
                        cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
                    }
                    cell.btnBookmark.tag = indexPath.row
                    cell.btnBookmark.addTarget(self, action: #selector(bookmarkButtonClicked(sender:)), for: .touchUpInside)
                    cell.btnMore.tag = indexPath.row
                    cell.btnMore.addTarget(self, action: #selector(moreButtonClicked(sender:)), for: .touchUpInside)
                    cell.lblName.text = dict["username"] as? String ?? ""
                    cell.lblTitle.text = dict["title"] as? String ?? ""
                    
                    let decodeText = dict["dicription"] as? String
                    let msg = String(describing: decodeText!.filter { !" \n\t\r".contains($0) })
                    if let decoded = msg.fromBase64(){
                        cell.lblDescription.text = decoded
                    }else{
                        cell.lblDescription.text = dict["dicription"] as? String
                    }
                    
                    cell.lblVoteCount.text = String.init(format: "\t\t %d Votes", dict["count_vote"] as! Int)
                    let date = GlobalConstant.getDateFromString(strDate: dict["add_date"] as! String)
                    cell.lblDateTime.text = Date().offset(from: date)
                    cell.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
                    if dict["like_status"] as! Int == 1 {
                        cell.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
                    }
                    cell.btnLike.setTitle(String.init(format: "%d", dict["count_like"] as! Int), for: .normal)
                    cell.btnLike.tag = indexPath.row
                    cell.btnLike.addTarget(self, action: #selector(likeButtonClicked(sender:)), for: .touchUpInside)
                    cell.btnComment.tag = indexPath.row
                    cell.btnComment.setTitle(String.init(format: "%d", dict["count_comment"] as! Int), for: .normal)
                    cell.btnComment.addTarget(self, action: #selector(commentButtonClicked(sender:)), for: .touchUpInside)
                    cell.btnViewAll.setTitle(String.init(format: "%d", dict["count_view"] as! Int), for: .normal)
                    cell.btnContact.tag = indexPath.row
                    cell.btnContact.addTarget(self, action: #selector(contactButtonClicked(sender:)), for: .touchUpInside)
                    cell.btnShare.tag = indexPath.row
                    cell.btnShare.addTarget(self, action: #selector(shareButtonClicked(sender:)), for: .touchUpInside)
                    
                    
                    if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
                        cell.btnLike.isEnabled = false
                        cell.btnComment.isEnabled = false
                        cell.btnShare.isEnabled = false
                        cell.btnBookmark.isEnabled = false
                        cell.btnMore.isEnabled = false
                        
                        cell.btnLike.alpha = 0.5
                        cell.btnComment.alpha = 0.5
                        cell.btnShare.alpha = 0.5
                        cell.btnBookmark.alpha = 0.5
                        cell.btnMore.alpha = 0.5
                    }
                    
                    
                    return cell
                    
                }
                
            }else{
                
                let index = Int(indexPath.row)
                let countt = realm?.objects(PostListRealm.self).filter("bucket == \(strBucketID)").count
                
                if(index < countt!){
                    let item = realm?.objects(PostListRealm.self).filter("bucket == \(strBucketID)")[index] as! PostListRealm
                    
                    print(item)
                    
                    
                    let post_type = item.post_type
                    if post_type == 1 {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! TableViewCell
                        
                        let bookmark_stat = item.bookmark_status
                        
                        if bookmark_stat == 0 {
                            cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                        } else {
                            cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
                        }
                        cell.btnBookmark.tag = indexPath.row
                        // cell.btnBookmark.addTarget(self, action: #selector(bookmarkButtonClicked(sender:)), for: .touchUpInside)
                        cell.btnMore.tag = indexPath.row
                        //  cell.btnMore.addTarget(self, action: #selector(moreButtonClicked(sender:)), for: .touchUpInside)
                        cell.lblName.text = item.username
                        cell.lblTitle.text = item.title
                        
                        
                        
                        let decodeText = item.dicription.description
                        let msg = String(describing: decodeText.filter { !" \n\t\r".contains($0) })
                        if let decoded = msg.fromBase64() {
                            cell.lblDescription.text = decoded
                        }else{
                            cell.lblDescription.text = item.dicription.description
                        }
                        
                        if(cell.lblDescription.text.count > 55){
                            cell.lblDescription.textContainer.maximumNumberOfLines = 2
                            cell.lblDescription.textContainer.lineBreakMode = .byTruncatingTail
                            cell.lblSeeMore.text = "See More"
                        }else{
                            cell.lblDescription.textContainer.lineBreakMode = .byWordWrapping
                            cell.lblSeeMore.text = ""
                        }
                        
                        
                        cell.imgBG.image = nil
                        cell.imgBG.af_cancelImageRequest()
                        
                        let postImg = item.post_image
                        if postImg != "" {
                            
                            cell.imgBG.af_setImage(withURL: URL.init(string: postImg)!, placeholderImage:UIImage.init(named: "image.jpg"))
                        }
                        let date = GlobalConstant.getDateFromString(strDate: item.add_date)
                        cell.lblDateTime.text = Date().offset(from: date)
                        
                        cell.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
                        
                        if item.like_status == 1 {
                            cell.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
                        }
                        cell.btnLike.setTitle(String.init(format: "%d", item.count_like), for: .normal)
                        cell.btnLike.tag = indexPath.row
                        //  cell.btnLike.addTarget(self, action: #selector(likeButtonClicked(sender:)), for: .touchUpInside)
                        cell.btnComment.tag = indexPath.row
                        cell.btnComment.setTitle(String.init(format: "%d", item.count_comment), for: .normal)
                        cell.btnComment.addTarget(self, action: #selector(commentButtonClicked(sender:)), for: .touchUpInside)
                        cell.btnViewAll.setTitle(String.init(format: "%d", item.count_view), for: .normal)
                        cell.btnContact.tag = indexPath.row
                        cell.btnContact.addTarget(self, action: #selector(contactButtonClicked(sender:)), for: .touchUpInside)
                        
                        
                        cell.btnShare.tag = indexPath.row
                        // cell.btnShare.addTarget(self, action: #selector(shareButtonClicked(sender:)), for: .touchUpInside)
                        
                        
                        if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
                            cell.btnLike.isEnabled = false
                            cell.btnComment.isEnabled = false
                            cell.btnShare.isEnabled = false
                            cell.btnBookmark.isEnabled = false
                            cell.btnMore.isEnabled = false
                            
                            cell.btnLike.alpha = 0.5
                            cell.btnComment.alpha = 0.5
                            cell.btnShare.alpha = 0.5
                            cell.btnBookmark.alpha = 0.5
                            cell.btnMore.alpha = 0.5
                        }
                        
                        return cell
                        
                    }else if post_type == 2{
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PollCell", for: indexPath) as! TableViewCell
                        if item.bookmark_status == 0 {
                            cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                        } else {
                            cell.btnBookmark.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
                        }
                        cell.btnBookmark.tag = indexPath.row
                        //  cell.btnBookmark.addTarget(self, action: #selector(bookmarkButtonClicked(sender:)), for: .touchUpInside)
                        cell.btnMore.tag = indexPath.row
                        //   cell.btnMore.addTarget(self, action: #selector(moreButtonClicked(sender:)), for: .touchUpInside)
                        cell.lblName.text = item.username
                        cell.lblTitle.text = item.title
                        
                        let decodeText = item.dicription.description
                        let msg = String(describing: decodeText.filter { !" \n\t\r".contains($0) })
                        if let decoded = msg.fromBase64(){
                            cell.lblDescription.text = decoded
                        }else{
                            cell.lblDescription.text = item.dicription.description
                        }
                        
                        cell.lblVoteCount.text = String.init(format: "\t\t %d Votes", item.count_vote)
                        let date = GlobalConstant.getDateFromString(strDate: item.add_date)
                        cell.lblDateTime.text = Date().offset(from: date)
                        cell.btnLike.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
                        if item.like_status == 1 {
                            cell.btnLike.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
                        }
                        cell.btnLike.setTitle(String.init(format: "%d",item.count_like), for: .normal)
                        cell.btnLike.tag = indexPath.row
                        //    cell.btnLike.addTarget(self, action: #selector(likeButtonClicked(sender:)), for: .touchUpInside)
                        cell.btnComment.tag = indexPath.row
                        cell.btnComment.setTitle(String.init(format: "%d", item.count_comment), for: .normal)
                        cell.btnComment.addTarget(self, action: #selector(commentButtonClicked(sender:)), for: .touchUpInside)
                        cell.btnViewAll.setTitle(String.init(format: "%d", item.count_view), for: .normal)
                        cell.btnContact.tag = indexPath.row
                        cell.btnContact.addTarget(self, action: #selector(contactButtonClicked(sender:)), for: .touchUpInside)
                        cell.btnShare.tag = indexPath.row
                          //cell.btnShare.addTarget(self, action: #selector(shareButtonClicked(sender:)), for: .touchUpInside)
                        
                        
                        if UserDefaults.standard.value(forKey: "userBlocked") as! Bool {
                            cell.btnLike.isEnabled = false
                            cell.btnComment.isEnabled = false
                            cell.btnShare.isEnabled = false
                            cell.btnBookmark.isEnabled = false
                            cell.btnMore.isEnabled = false
                            
                            cell.btnLike.alpha = 0.5
                            cell.btnComment.alpha = 0.5
                            cell.btnShare.alpha = 0.5
                            cell.btnBookmark.alpha = 0.5
                            cell.btnMore.alpha = 0.5
                        }
                        return cell
                    }
                }
            }
        }
        else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddCell", for: indexPath) as! TableViewCell
                return cell
            }
            return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none {
            if indexPath.row == arrPostList.count-1 {
                if self.isthereMoreData == true {
                    self.getPostByBucketID(pooltoRefresh: false)
                    self.lblNoFeed.isHidden = true
                }else{
                    self.lblNoFeed.isHidden = false
                }
            }
        }
        else{
            let allItems = DBManager.sharedInstance.getDataFromDB()
            let kkk = allItems.filter("bucket == \(strBucketID)", self)
            if indexPath.row == kkk.count-1 {
                
                self.lblNoFeed.isHidden = false
                let alert = UIAlertController(title: GlobalConstant.AppName, message: "No internet connection", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    
    
    
    func scrollViewDidScroll(_ tableview: UIScrollView) {
        if tableview == tblViewList  {
            let indexPaths = tblViewList.indexPathsForVisibleRows
            for indexPath in indexPaths!{
                DispatchQueue.main.async {
                    if (indexPath.row == 0) || (indexPath.row == 1) || (indexPath.row == 2) || (indexPath.row == 3) || (indexPath.row == 4){
                        self.btnScrollTop.isHidden = true
                    }else{
                        self.btnScrollTop.isHidden = false
                    }
                }
            }

        }
    }
    
    
    @objc func contactButtonClicked(sender: UIButton)  {
        
        
        
        if(self.arrPostList.count>0){

        if reach.connection == .cellular || reach.connection == .wifi || reach.connection != .none{

                tblRowcount = sender.tag
                let dict  = self.arrPostList[sender.tag] as! NSDictionary
                if dict["post_type"] as! Int == 1 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                    polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }

                if dict["post_type"] as! Int == 2 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PollDetailViewController") as! PollDetailViewController
                    polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }
        }
        else{
                let index = Int(sender.tag)


                let allItems = DBManager.sharedInstance.getDataFromDB()
                let kkk = allItems.filter("bucket == \(strBucketID)", self)
                let item = kkk[index]
                 print(item)
                    //DBManager.sharedInstance.getDataFromDB()[index] as PostListRealm

                tblRowcount = index

                if item.post_type == 1 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                    polldetail.postIndex = index
                    polldetail.strBucketID = strBucketID
                    polldetail.strPostId = String.init(format: "%d", item.post_id)
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }
                if item.post_type == 2 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PollDetailViewController") as! PollDetailViewController
                    polldetail.strPostId = String.init(format: "%d", item.post_id)
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }
            }
        }
    }
    
    @objc func bookmarkButtonClicked(sender: UIButton) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
        
        let dict  = self.arrPostList[sender.tag] as! NSDictionary
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dict["post_id"] as! Int)]
        
        appdelegate.vibrateMobile()
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)Add_bookmark", strType: "Add_bookmark", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            
            if response.count == 0 {
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            
                            if response["Data"] as! String == "Like"{
                                sender.setImage(#imageLiteral(resourceName: "favorite_feel"), for: .normal)
                            } else {
                                sender.setImage(#imageLiteral(resourceName: "favorite"), for: .normal)
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
        }
    }
    @objc func moreButtonClicked(sender: UIButton)  {
        let dict  = self.arrPostList[sender.tag] as! NSDictionary
        let dropDown = DropDown()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        if Int(UserDefaults.standard.value(forKey: "userid") as! String) == dict["user_id"] as? Int {
            if dict["post_type"] as! Int == 1 {
                // The list of items to display. Can be changed dynamically
                dropDown.dataSource = ["Delete Post", "Edit Post"]
            }
            else{
                // The list of items to display. Can be changed dynamically
                dropDown.dataSource = ["Delete Post"]
            }
        }
        else{
            dropDown.dataSource = ["Report Abuse"]
        }
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if index == 0 {
                if item == "Report Abuse" {
                    if dict["report_abuse_status"] as! Int == 0 {
                        self.reportAbuseAPICalled(dictPost: dict)
                    }
                    else{
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Already report this post.", on: self)
                    }
                }
                else{
                    if Int(UserDefaults.standard.value(forKey: "userid") as! String) == dict["user_id"] as? Int {
                        self.deletePostAlert(dictPost: dict)
                    }
                }
            }
            else{
               
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatePostVC") as! CreatePostVC
                vc.dictPost = dict
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        dropDown.show()
    }
    
    func reportAbuseAPICalled(dictPost:NSDictionary) {
        
        dictDetail = dictPost
        self.abuseTextview.text = "Write here..."
        self.abuseTextview.textColor = UIColor.lightGray
        self.reportToAbuseViuew.isHidden = false
    }
    
    func deletePostAlert(dictPost:NSDictionary) {
        
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: GlobalConstant.AppName, message: "Are you sure you want to delete this post?", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Yes", style: .cancel) { action -> Void in
            //Do some stuff
            self.deletePostApiCalled(dict: dictPost)
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "No", style: .default) { action -> Void in
            //Do some other stuff
        }
        actionSheetController.addAction(nextAction)
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func deletePostApiCalled(dict :NSDictionary) {
        let dictParam : NSDictionary = ["post_id":String.init(format: "%d", dict["post_id"] as! Int)]
        
        WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)delete_post", strType: "delete_post", dictParameter: dictParam, controllerView: self, success: { (response) in
            print(response)
            
            if response.count == 0 {
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            }
            else{
                if response["Status"] is Int{
                    if response["Status"] as! Int == 200 {
                        DispatchQueue.main.async {
                            
                            self.arrPostList.remove(dict)
                            self.tblViewList.reloadData()
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                        }
                    }
                }
            }
        }, failure: { (error) in
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    

    
    @objc func likeButtonClicked(sender: UIButton)  {
        
        let count = Int(sender.title(for: .normal)!)
        if (sender.currentImage == #imageLiteral(resourceName: "like_Blue")){
            sender.setImage(#imageLiteral(resourceName: "like_dash"), for: .normal)
            if count != 0 {
                sender.setTitle("\(count!-1)", for: .normal)
            }
        }
        else{
            sender.setImage(#imageLiteral(resourceName: "like_Blue"), for: .normal)
            sender.setTitle("\(count!+1)", for: .normal)
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            let dict  = self.arrPostList[sender.tag] as! NSDictionary
            let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!,"post_id":String.init(format: "%d", dict["post_id"] as! Int)]
            
            
            DispatchQueue.main.async {
                appdelegate.vibrateMobile()
                
                WebHelper.requestUrlForPostWithType("\(GlobalConstant.PostURL)like_post", strType: "like_post", dictParameter: dictParam, controllerView: self, success: { (response) in
                    print(response)
                    
                    if response.count == 0 {
                        DispatchQueue.main.async {
                            
                            GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                        }
                    }
                    else{
                        if response["Status"] is Int{
                            if response["Status"] as! Int == 200 {
                                DispatchQueue.main.async {
                                    
                                    if response["Data"] as! String == "Like"{
                                        
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                            }
                            else{
                                DispatchQueue.main.async {
                                    
                                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: response["Data"] as! String, on: self)
                                }
                            }
                        }
                    }
                }, failure: { (error) in
                    DispatchQueue.main.async {
                        
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
                    }
                })
            }
        }
        
    }
    
   
    @objc func shareButtonClicked(sender: UIButton)  {
        
            let dict  = self.arrPostList[sender.tag] as! NSDictionary
            let text = dict["title"] as! String
            let userId = UserDefaults.standard.value(forKey: "userid") as! String
            let post_id = dict["post_id"] as! Int
            
            let ios_url = "\(GlobalConstant.PostURL)poll_detail?post_id=\(post_id)"
        
        
//        let alias = "Share-Alias-Link-Example"
//        let canonicalIdentifier = alias
//
//        let shareBranchObject = BranchUniversalObject.init(canonicalIdentifier: canonicalIdentifier)
//        shareBranchObject.title = "Share Branch Link Example"
//        shareBranchObject.canonicalIdentifier = "614710861957127740"
//        shareBranchObject.canonicalUrl = "https://h1bq.com/"
//        shareBranchObject.imageUrl = "https://branch.io/img/press/kit/badge-black.png"
//        shareBranchObject.keywords = [ "example", "short", "share", "link" ]
//        shareBranchObject.contentDescription = "This is an example shared alias link."
//
//        let shareLinkProperties = BranchLinkProperties()
//        shareLinkProperties.alias = alias
//        shareLinkProperties.controlParams = ["$fallback_url": "https://itunes.apple.com/us/app/storyboard-podcasts/id1409517363?mt=8"]
//
//        let branchShareLink = BranchShareLink.init(
//            universalObject: shareBranchObject,
//            linkProperties:  shareLinkProperties
//        )
//        branchShareLink.shareText = "Shared with TestBed-Swift"
//        branchShareLink.delegate = self
//        let activityViewController = UIActivityViewController.init(
//            activityItems: branchShareLink.activityItems(),
//            applicationActivities: nil
//        )
//        self.present(activityViewController, animated: true, completion: nil)
        
        
        let buo = BranchUniversalObject.init(canonicalIdentifier: "content/12345")
        buo.title = "H1BQ"
        buo.contentDescription = ""
        


        let lp: BranchLinkProperties = BranchLinkProperties()
        
        lp.channel = "WhatsApp"
        lp.feature = "sharing"
        lp.stage = "new user"
        lp.tags = ["like","share","comment"]
        
        
        
       // "+url": "https://ukxah1bq.app.link/gH2SEBRxzT",
        

        lp.addControlParam("$desktop_url", withValue: "http://example.com/desktop")
        lp.addControlParam("$canonical_url", withValue: "https://h1bq.com/")
        lp.addControlParam("$one_time_use", withValue: "false")
        lp.addControlParam("$ios_url", withValue: "\(ios_url)/ios")
        lp.addControlParam("$ipad_url", withValue: "http://example.com/ios")
        
        lp.addControlParam("$deeplink_path", withValue: "content/1234")
        //lp.addControlParam("$fallback_url", withValue: "https://itunes.apple.com/us/app/storyboard-podcasts/id1409517363?mt=8")


        buo.getShortUrl(with: lp) { (url, error) in
            print(url ?? "")
        }
        
        
        

        
        let message = "Check out this link"
        buo.showShareSheet(with: lp, andShareText: message, from: self) { (activityType, completed) in
            print(activityType ?? "")
        }
        
//            let branchUniversalObjec : BranchUniversalObject = BranchUniversalObject()
//            let linkProperties: BranchLinkProperties = BranchLinkProperties()
//            linkProperties.feature = "sharing "
//            //linkProperties.
//            linkProperties.addControlParam("$desktop_url", withValue: "https://h1bq.com/home")
//            linkProperties.addControlParam("$ios_url", withValue: "\(ios_url)/ios")
        
//            branchUniversalObjec.showShareSheet(with: linkProperties,
//                                                andShareText: "\(text)\n",
//            from: self) { (activityType, completed) in
//                NSLog("done showing share sheet!")
//            }
        
     }
    
    
    @objc func commentButtonClicked(sender: UIButton)  {
        if(self.arrPostList.count>0){
            
            if(isInternetConnection == "true"){
                tblRowcount = sender.tag
                let dict  = self.arrPostList[sender.tag] as! NSDictionary
                if dict["post_type"] as! Int == 1 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                    polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
                    
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }
                
                if dict["post_type"] as! Int == 2 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PollDetailViewController") as! PollDetailViewController
                    polldetail.strPostId = String.init(format: "%d", dict["post_id"] as! Int)
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }
            }
            else{
                
                
                let index = Int(sender.tag)
                let allItems = DBManager.sharedInstance.getDataFromDB()
                let kkk = allItems.filter("bucket == \(strBucketID)", self)
                let item = kkk[index]
                tblRowcount = index
                
                if item.post_type == 1 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                    polldetail.strPostId = String.init(format: "%d", item.post_id)
                    polldetail.postIndex = index
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }
                if item.post_type == 2 {
                    let polldetail = self.storyboard?.instantiateViewController(withIdentifier: "PollDetailViewController") as! PollDetailViewController
                    polldetail.strPostId = String.init(format: "%d", item.post_id)
                    self.navigationController?.pushViewController(polldetail, animated: true)
                }
            }
        }
        
        
    }
}
extension HomeVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write here..." {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Write here..."
            textView.textColor = UIColor.lightGray
        }
    }
}
extension HomeVC: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    
    
    func callAPIForCheckActiveORnot() {
        
        let dictParam : NSDictionary = ["user_id":UserDefaults.standard.value(forKey: "userid") as! String!]
        
        WebHelper.requestForCheckUsernamePostUrl("\(GlobalConstant.checkActiveORnot)", dictParameter: dictParam, controllerView: self, success: { (response) in
            
            print(response)
            
            if response.count == 0 {
                
                DispatchQueue.main.async {
                    
                    GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Server error, Please try again letter!", on: self)
                }
            } else {
                
                if response["Status"] as! Int == 200 {
                    print(response)
                    DispatchQueue.main.async {
                        
                         let active : String = String((response["Data"] as! NSDictionary)["user_status"]!  as! NSString)
                         print(active)
                        if active == "0"{
                            UserDefaults.standard.set(true, forKey: "userBlocked")
                            appdelegate.homeButtonVar.isEnabled = false
                            appdelegate.homeButtonVar.alpha = 0.5
                        }else{
                            appdelegate.homeButtonVar.isEnabled = true
                            appdelegate.homeButtonVar.alpha = 1.0
                            UserDefaults.standard.set(false, forKey: "userBlocked")
                        }
                    }
                } else {
                    
                    DispatchQueue.main.async {
                        
                        GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: "Invalid login!", on: self)
                    }
                }
            }
        }, failure: { (error) in
            
            DispatchQueue.main.async {
                
                GlobalConstant.showAlertMessage(withOkButtonAndTitle: GlobalConstant.AppName, andMessage: (error?.localizedDescription)!, on: self)
            }
        })
    }
    
    
    
    //// IPV6 config code
    func ipv6Reachability() -> SCNetworkReachability?{
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
}


extension String {
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.endIndex.encodedOffset)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.endIndex.encodedOffset
        } else {
            return false
        }
    }
    
    func highlightWordsIn(highlightedWords: String, attributes: [[NSAttributedStringKey: Any]]) -> NSMutableAttributedString {
        let range = (self as NSString).range(of: highlightedWords)
        let result = NSMutableAttributedString(string: self)
        
        for attribute in attributes {
            result.addAttributes(attribute, range: range)
        }
        
        return result
    }
}
